	<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('Abstract_model.php');

class Subscriber_model extends Abstract_model {

    protected $table_name = "";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

    function __construct() 
    {
        $this->table_name = "subscriber";
		parent::__construct();
    }

	
}
?>