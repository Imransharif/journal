<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('Abstract_model.php');

class Admin_model extends Abstract_model {

    protected $table_name = "";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "login";
		parent::__construct();
    }
 
	public function login($mail, $password,$status)
	{
		$this->db->select();
		$this->db->from($this->table_name);
		$this->db->where('email',$mail);
		$this->db->where('password',$password);
		$this->db->where('status',$status);
		$data= $this->db->get();  
	    if($data->num_rows()>0) 
	    {
	   		return $data->result_array();
	    }
	    else
		{
			return false;
		}
	}

	public  function  comparemail($email)
	{
		$this->db->where('email', $email);  
		$query = $this->db->get($this->table_name);  
		
			if($query->num_rows() > 0)  
			{  
				return true;  
			}  
			else  
			{  
			    return false;  
			}           
	} 
	
}
?>