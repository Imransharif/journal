
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/user_dashboard';
		$this->load->model('Slider_model');
		$this->load->model('Vision_mission_model');
		$this->load->model('Department_model');
		$this->load->model('Student_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('News_model');
		
	}

	public function index()
	{
		$data= array();
	    $where = "dep_status =1";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'left'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['feature_department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);
		$data['key']=$this->Slider_model->get_where('*','','', true, '', '', '');
	    $data['vission']=$this->Vision_mission_model->get_where('*','','', true, '', '', '');
	    $data['count']=$this->Department_model->count_data();
	    $data['studentcount']=$this->Student_model->count_data();
	    $data['news']=$this->News_model->get_where('*','',true, 'id DESC', 4,'');
        $this->load->view('userhome/home',$data);
	
    }
    

	}
