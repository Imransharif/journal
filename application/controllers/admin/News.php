
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('News_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('news/add_news');
			}

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_news()
	{
		$this->layout = 'admin/user_dashboard';
		$data['news']=$this->News_model->get_where('*','','', true, '', '', '');
		$this->load->view('news/user_side_news',$data);

	}
	public function show_user_side_single_news($id)
	{

		$this->layout = 'admin/user_dashboard';
		$where = (!empty($id))?"id = '".$id."' ":"id > 0 ";
		$data['remaining_news']=$this->News_model->not_equal($id);
		$data['single_news']=$this->News_model->get_where('*',$where,'', true, '', '', '');
		$this->load->view('news/single_news',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}


	public function show_news()
	{
		
		if($this->session->userdata('user_email'))
		{    


			if($this->session->userdata('status')=='Director_Qec')
			{

				$data['news']=$this->News_model->get_where('*','',true, 'id DESC', '', '');
				$this->load->view('news/show_news',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{


		if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
		{
			$config= array();
			$config['upload_path'] = FCPATH.'images/';
			$config['allowed_types'] = 'gif|jpg|png|mp4';
			$this->load->library('upload',$config);
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) 
			{
				$image = $data['file_name']; 

			}
			else
			{
				echo $this->upload->display_errors();
			}
		}

		if($this->session->userdata('user_email'))
		{

			$nophoto = "nophoto.png";
			$data = array(
				'news_heading' =>$this->input->post('news_heading'),
				'news_description' =>$this->input->post('department_desc'),
				'news_date' =>$this->input->post('news_date'),
				'image' =>!empty($image)?$image:$nophoto

			);
			$data1=$this->News_model->save($data);
			redirect('admin/News/show_news');	
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}




	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->News_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/News/show_news');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['data']=$this->News_model->get_by('id',$id);
				$this->load->view('news/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}



		
		
		public function update()
		{
			if($this->session->userdata('user_email'))
			{
				if($this->session->userdata('status')=='Director_Qec' )
				{
					if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
					{
						$id=$this->input->post('id');
						$config= array();
						$config['upload_path'] = FCPATH.'images/';
						$config['allowed_types'] = 'gif|jpg|png|mp4';
						$this->load->library('upload',$config);
						$this->upload->do_upload('photo');
						$data = $this->upload->data();
						if($data) 
						{
							$image = $data['file_name']; 

						}
						else
						{
							echo $this->upload->display_errors();
						}
					}

					$temp_img= $this->input->post('temp_img');
					$data = array(
						'news_heading' =>$this->input->post('news_heading'),
						'news_description' =>$this->input->post('department_desc'),
						'news_date' =>$this->input->post('news_date'),
						'image' =>!empty($image)?$image:$temp_img

					);
					$data=$this->News_model->update_by('id',$id,$data);
					redirect('admin/News/show_news');	


				}
			}
			else
			{
				redirect('admin/Login/loginload');
			}



		}
	}