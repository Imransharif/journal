<?php 
class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Web_owner_info_model');
		$this->load->model('Director_pb_model');
		$this->load->model('Manager_model');
		$this->load->model('Coordinator_model');
		$this->load->model('Teacher_model');
		$this->load->model('Student_model');
		$this->load->library('form_validation');
		
		
	}

	public function loginload()
	{
		
		$this->layout = "";
		$this->load->view('login/login');
		
	}
	


	public function login()
	{  
		
		$username=$this->input->post('email');
		$password=$this->input->post('password');
		$status=$this->input->post('status');

		if($status=='Director_Qec')
		{
			
			$data=$this->Web_owner_info_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'logo' =>$data[0]['logo'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}
		if($status=='Director_Pb')
		{
			$data=$this->Director_pb_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'image' =>$data[0]['image'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}
		if($status=='Manager')
		{
			$data=$this->Manager_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'image' =>$data[0]['image'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}
		if($status=='Cordinator')
		{
			$data=$this->Coordinator_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'image' =>$data[0]['image'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}
		if($status=='Teacher')
		{
			$data=$this->Teacher_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'image' =>$data[0]['image'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}
		if($status=='Student')
		{
			$data=$this->Student_model->login($username,$password,$status);
			if($data)
			{
				$arraydata =  array(
					'user_email' =>$data[0]['email'],
					'password' =>$data[0]['password'],
					'image' =>$data[0]['image'],
					'name' =>$data[0]['name'],
					'email' =>$data[0]['email'],
					'status' =>$data[0]['status']
				);
				$this->session->set_userdata($arraydata);
				redirect('admin/Login/deshboard');
			}
			else
			{       
				$this->session->set_flashdata('error_message','Invalid email or password');
				redirect('admin/Login/loginload');
			}
		}

		
		

	}

	public function forgetpassword_load()
	{ 

		$this->layout = "";
		$this->load->view('login/resetpassword');
	} 


	public function logout()
	{ 

		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('password');
		$this->session->unset_userdata('status');
		redirect('admin/Login/loginload');

	}
	public function deshboard()
	{
		$this->load->view('home/dashboard');
	}

	// public function available_email()
	// {
	// 	if($this->session->userdata('user_email'))
	// 	{

	// 		$this->layout = "";
	// 		if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL))  
	// 		{  
	// 			echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Invalid Email</span></label>';
	// 		}
	// 		else
	// 		{
	// 			echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span>valid Email</span></label>';  
	// 		} 
	// 	} 
	// 	else
	// 	{
	// 		redirect('admin/Login/loginload');
	// 	}
	
	// }

	
	
	// public function setpassword($code)
	// {

	// 	$this->layout = "";
	// 	$data['code']=$code;
	// 	$this->load->view('Login/resetpasword',$data);	
	// }

	// public function sendmailfoam()
	// {
	
	// 	$this->layout = "";
	// 	$this->load->view('Login/sendmail');
	
	
	// }

	// public function forgetpasswordfoam()
	// {
	// 	if($this->session->userdata('user_email'))
	// 	{

	// 		$this->layout = "";
	// 		$this->load->view('Login/sendmail');
	// 	} 
	// 	else
	// 	{
	// 		redirect('admin/Login/loginload');
	// 	}

	// }

	




} 



?>