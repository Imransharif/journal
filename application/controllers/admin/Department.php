
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Category_model');
		$this->load->model('Programe_model');
		$this->load->model('Department_model');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['category']=$this->Category_model->get_where('*','', true, '', '', '');
				$this->load->view('department/adddepartment',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_department()
	{
		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=array();
				$joins = array(
					'0' => array('table_name' => 'category category',
						'join_on' => ' category.id = department.category_id ',
						'join_type' => 'inner'
					)
				);
				$from_table = "department department";
				$select_from_table = 'department.*,category.*';
				$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, '', '','', '', '', '','', '', '',true);
				$this->load->view('department/show_department',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}

	public function single_department($department_id='')
	{

		$this->layout = 'admin/user_dashboard';
		$where = (!empty($department_id))?"department_id = '".$department_id."' ":"department_id > 0 ";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => 'category.id = department.category_id ',
				'join_type' => 'inner'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['single_department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);

		$data['remaining_department']=$this->Department_model->not_equal($department_id);
		$this->load->view('department/single_department',$data);

	}

	public function insertion()
	{


		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}
				$p_point = $this->input->post('p_point');
				$p_p_desc = $this->input->post('p_p_desc');
				$p = implode(',' ,$p_point);
				$d = implode(',' ,$p_p_desc);
				$thumbnailimage='no image';
				$data = 
				array(
					'heading' =>$p,
					'paragraph' =>$d,
					'category_id' => $this->input->post('category_name'),
					'department_name' => $this->input->post('department_name'),
					'department_desc' => $this->input->post('department_desc'),
					'programe_id' =>$this->input->post('programe_name'),
					'image' =>        !empty($image)?$image:$thumbnailimage,
				);


				$data1=$this->Department_model->save($data);
				redirect('admin/Department/show_department');	
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}
		

	}


	public function update_status()
	{
		$this->layout = '';
		$data = array();
		if($this->session->userdata('user_email'))
		{
			$id=$this->input->post('value');
			$status=$this->input->post('status');

			if($status==0)
			{
				$data['dep_status'] = 1;

			}
			else
			{
				$data['dep_status'] = 0;
			}
			
			$this->Department_model->update_by('department_id',$id,$data); 
			$data['id'] =$id;
			echo json_encode($data);
		}
		
	}






	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Department_model->delete_by('department_id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Department/show_department');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{

			$where = (!empty($id))?"department_id = '".$id."' ":"department_id > 0 ";
			$joins = array(
				'0' => array('table_name' => 'category category',
					'join_on' => ' category.id = department.category_id ',
					'join_type' => 'inner'
				)
			);
			$from_table = "department department";
			$select_from_table = 'department.*,category.*';
			$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);
			$data['category']=$this->Category_model->get_where('*','', true, '', '', '');
			$this->load->view('department/edit',$data);
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	public function view($id)
	{
		if($this->session->userdata('user_email'))
		{
			

			$where = (!empty($id))?"department_id = '".$id."' ":"department_id > 0 ";
			$joins = array(
				'0' => array('table_name' => 'category category',
					'join_on' => ' category.id = department.category_id ',
					'join_type' => 'inner'
				)
			);
			$from_table = "department department";
			$select_from_table = 'department.*,category.*';
			$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);
			$data['category']=$this->Category_model->get_where('*','', true, '', '', '');
			$this->load->view('department/view',$data);
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	



		public function update()
		{
			if($this->session->userdata('user_email'))
			{
				
				$id=$this->input->post('id');
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}
				$p_point = $this->input->post('p_point');
				$p_p_desc = $this->input->post('p_p_desc');
				$p = implode(',' ,$p_point);
				$d = implode(',' ,$p_p_desc);
				$tmp_img= $this->input->post('tmp_img');
				$data = 
				array(
					'heading' =>$p,
					'paragraph' =>$d,
					'category_id' => $this->input->post('category_name'),
					'department_name' => $this->input->post('department_name'),
					'department_desc' => $this->input->post('department_desc'),
					'programe_id' =>$this->input->post('programe_name'),
					'image' =>        !empty($image)?$image:$tmp_img,
				);


				$data=$this->Department_model->update_by('department_id',$id,$data);
				redirect('admin/Department/show_department');	
			} 
			else
			{
				redirect('admin/Login/loginload');
			}
		}


	
	}
