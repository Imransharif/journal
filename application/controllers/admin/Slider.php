
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller 
{

  public function __construct()
  {
    parent::__construct();
    $this->layout = 'admin/admin_dashborad';
    $this->load->model('Manager_model');
    $this->load->model('Web_owner_info_model');
    $this->load->model('Slider_model');
    $this->load->model('Department_model');
    
  }

  public function index()
  {

    if($this->session->userdata('user_email'))
    {
      $this->load->view('slider/add_slider');
    } 
    else
    {
      redirect('admin/Login/loginload');
    }
    
  }

  public function show_user_side_slider()
  {

    $data['key']=$this->Slider_model->get_where('*','','', true, '', '', '');
    $this->load->view('userhome/home',$data);

  }

  public function showcategory()
  {

    $this->load->view('category/showcategory');
    
  }


  public function show_slider()
  {
    if($this->session->userdata('user_email'))
    {

      if($this->session->userdata('status')=='Director_Qec')
      {

        $data['key']=$this->Slider_model->get_where('*','','', true, '', '', '');
        $this->load->view('slider/show_slider',$data);

      }

    }

    else
    {
      redirect('admin/Login/loginload');
    }

    
  }

  public function insertion()
  {


    if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
    {
      $config= array();
      $config['upload_path'] = FCPATH.'images/';
      $config['allowed_types'] = 'gif|jpg|png|mp4';
      $this->load->library('upload',$config);
      $this->upload->do_upload('photo');
      $data = $this->upload->data();
      if($data) 
      {
        $image = $data['file_name']; 

      }
      else
      {
        echo $this->upload->display_errors();
      }
    }

    if($this->session->userdata('user_email'))
    {
      $nophoto = "nophoto.png";
      $data = array(
        'heading' =>$this->input->post('heading'),
        'paragraph' =>$this->input->post('department_desc'),
        'image' =>!empty($image)?$image:$nophoto,
        
      );
      $data1=$this->Slider_model->save($data);
      redirect('admin/Slider/show_slider'); 
    } 
    else
    {
      redirect('admin/Login/loginload');
    }
    

  }





  public function delete($id)
  {
    if($this->session->userdata('user_email'))
    {

      $data=$this->Slider_model->delete_by('id',$id);
      if($data)
      {
        $this->session->set_flashdata('delete','mymsg');
        redirect('admin/Slider/show_slider'); 
      }
    } 
    else
    {
      redirect('admin/Login/loginload');
    }

  }
  public function edit($id)
  {
    if($this->session->userdata('user_email'))
    {

      $data['data']=$this->Slider_model->get_by('id',$id);
      $this->load->view('slider/edit',$data);
    } 
    else
    {
      redirect('admin/Login/loginload');
    }
    
  }
  


    public function update()
    {
     

      if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
      {
        $config= array();
        $config['upload_path'] = FCPATH.'images/';
        $config['allowed_types'] = 'gif|jpg|png|mp4';
        $this->load->library('upload',$config);
        $this->upload->do_upload('photo');
        $data = $this->upload->data();
        if($data) 
        {
          $image = $data['file_name']; 

        }
        else
        {
          echo $this->upload->display_errors();
        }
      }

      $temp_image= $this->input->post('temp_image');
       $id=$this->input->post('id');
      $data = array(
        'heading' =>$this->input->post('heading'),
        'paragraph' =>$this->input->post('department_desc'),
        'image' =>!empty($image)?$image:$temp_image,
        
      );
      $data=$this->Slider_model->update_by('id',$id,$data);
      if($data)
      {
        $this->session->set_flashdata('update','mymsg');
        redirect('admin/Slider/show_slider');  
      } 


      else
      {
        redirect('admin/Login/loginload');
      }

    }


 
  }
