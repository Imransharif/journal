
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Events_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('events/add_events');
			}

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_events()
	{
		$this->layout = 'admin/user_dashboard';
		$data['events']=$this->Events_model->get_where('*','', true, 'id DESC', '', '');
		$this->load->view('events/show_user_side_events',$data);

	}

	public function showcategory()
	{
		if($this->session->userdata('status')=='Director_Qec' )
		{
			$this->load->view('category/showcategory');
		}
		
	}


	public function show_events()
	{
		
		if($this->session->userdata('user_email'))
		{   

			if($this->session->userdata('status')=='Director_Qec')
			{

				$data['key']=$this->Events_model->get_where('*','', true, 'id DESC', '', '');
				$this->load->view('events/show_events',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				if (isset($_FILES['banner_image']['name']) && $_FILES['banner_image']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('banner_image');
					$data = $this->upload->data();
					if($data) 
					{
						$banner_image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}
				if (isset($_FILES['thumbnail_image']['name']) && $_FILES['thumbnail_image']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('thumbnail_image');
					$data = $this->upload->data();
					if($data) 
					{
						$thumbnail_image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				$gotherearray=$this->input->post('gothere');
				$gotherestring=implode(',',$gotherearray);

				$doneedarray=$this->input->post('doneed');
				$doneedstring=implode(',',$doneedarray);
				$nophoto = "nophoto.png";
				$data = array(
					'event_name' =>$this->input->post('event_name'),
					'event_places' =>$this->input->post('event_places'),
					'event_startdate' =>$this->input->post('event_startdate'),
					'event_enddate' =>$this->input->post('event_enddate'),
					'banner_heading' =>$this->input->post('banner_heading'),
					'banner_paragraph' =>$this->input->post('banner_paragraph'),
					'event_desc' =>$this->input->post('department_desc'),
					'gothere' =>$gotherestring,
					'doneed' =>$doneedstring,
					'event_banner_image' =>!empty($banner_image)?$banner_image:$nophoto,
					'event_thumbnail_image' =>!empty($thumbnail_image)?$thumbnail_image:$nophoto,

				);
				$data1=$this->Events_model->save($data);
				redirect('admin/Events/show_events');	
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}


	public function single_events()
	{

		$this->load->view('events/single_events');


	}


	public function get_specific_events($id)
	{

		$this->layout = 'admin/user_dashboard';
		$data['events']=$this->Events_model->get_by('id',$id);
		$this->load->view('events/single_events',$data);
	} 
	

	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Events_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Events/show_events');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
			$data['data']=$this->Events_model->get_by('id',$id);
			$this->load->view('events/edit',$data);
		    }
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function view($id)
	{
		if($this->session->userdata('user_email'))
		{
            if($this->session->userdata('status')=='Director_Qec' )
			{
			$data['data']=$this->Events_model->get_by('id',$id);
			$this->load->view('events/view',$data);
		    }
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}

		
		
		public function update()
		{
			if($this->session->userdata('user_email'))
			{

				$id=$this->input->post('id');
				if (isset($_FILES['banner_image']['name']) && $_FILES['banner_image']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('banner_image');
					$data = $this->upload->data();
					if($data) 
					{
						$banner_image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}
				if (isset($_FILES['thumbnail_image']['name']) && $_FILES['thumbnail_image']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('thumbnail_image');
					$data = $this->upload->data();
					if($data) 
					{
						$thumbnail_image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				if($this->session->userdata('user_email'))
				{
					$gotherearray=$this->input->post('gothere');
					$gotherestring=implode(',',$gotherearray);

					$doneedarray=$this->input->post('doneed');
					$doneedstring=implode(',',$doneedarray);
					$event_banner_image= $this->input->post('banner_image');
					$evesnt_thumbnail_image= $this->input->post('thumbnail_image');
					$data = array(
						'event_name' =>$this->input->post('event_name'),
						'event_places' =>$this->input->post('event_places'),
						'event_startdate' =>$this->input->post('event_startdate'),
						'event_enddate' =>$this->input->post('event_enddate'),
						'banner_heading' =>$this->input->post('banner_heading'),
						'banner_paragraph' =>$this->input->post('banner_paragraph'),
						'event_desc' =>$this->input->post('department_desc'),
						'gothere' =>$gotherestring,
						'doneed' =>$doneedstring,
						'event_banner_image' =>!empty($banner_image)?$banner_image:$event_banner_image,
						'event_thumbnail_image' =>!empty($thumbnail_image)?$thumbnail_image:$evesnt_thumbnail_image,

					);
					$data=$this->Events_model->update_by('id',$id,$data);
					redirect('admin/Events/show_events');	
				}
			} 
			else
			{
				redirect('admin/Login/loginload');
			}



		}
	}