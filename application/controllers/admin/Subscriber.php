
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriber extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Subscriber_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			$this->load->view('news/add_news');

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_news()
	{
		$this->layout = 'admin/user_dashboard';
		$data['news']=$this->News_model->get_where('*','','', true, '', '', '');
		$this->load->view('news/user_side_news',$data);

	}
	public function show_user_side_single_news($id)
	{

		$this->layout = 'admin/user_dashboard';
		$where = (!empty($id))?"id = '".$id."' ":"id > 0 ";
		$data['remaining_news']=$this->News_model->not_equal($id);
		$data['single_news']=$this->News_model->get_where('*',$where,'', true, '', '', '');
		$this->load->view('news/single_news',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}


	public function show_subscriber()
	{
		
		if($this->session->userdata('user_email'))
		{   

			if($this->session->userdata('status')=='Director_Qec')
			{

				$data['subscriber']=$this->Subscriber_model->get_where('*','',true, 'id DESC', '', '');
				$this->load->view('subscriber/show_subscriber',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{

		$data = array(
			'email' =>$this->input->post('value'),
		);
		$data1=$this->Subscriber_model->save($data);
			

	}


	public function datables()
	{
		if($this->session->userdata('user_email'))
		{

			$draw = intval($this->input->get('draw'));
			$start = intval($this->input->get('start'));
			$length = intval($this->input->get('length'));
			$search=$this->input->get('search');
			$order=$this->input->get('order');
			$columns=$this->input->get('columns');
			$start = $start?$start:$start;
			if($length)
				$this->db->limit($length);
			$this->db->offset($start);
			$value = '';
			if(isset($search['value']) && !empty($search['value']))
			{
				$value = $search['value'];

			}

			if(isset($order[0]['column']))
			{
				$order_column=$order[0]['column'];
				$order_dir = $order[0]['dir'];
				$column_name = $columns[$order_column]['data'];
				$this->db->order_by($column_name,$order_dir);
			}

			$show_data = $this->Tour_model->tourdata($value);
			$count_record=$this->Tour_model->get_total();
			$response['draw']= $draw;
			$response['recordsTotal']= $count_record;
			$response['recordsFiltered'] = $count_record;
			$response['data'] = $show_data;
			echo json_encode($response);
			exit;
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function home()
	{
		if($this->session->userdata('user_email'))
		{

			$this->load->view('home/home');
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function pagination($per_page,$page_url,$total_rows)

	{
		$this->load->library('pagination');
		$config['base_url']=base_url($page_url);
		$config['per_page']=$per_page;
		$config['total_rows']=$total_rows;
		$config["use_page_numbers"] = TRUE;
		$config["full_tag_open"] = '<ul class="pagination">';
		$config["full_tag_close"] = '</ul>';
		$config["first_tag_open"] = '<li>';
		$config["first_tag_close"] = '</li>';
		$config["last_tag_open"] = '<li>';
		$config["last_tag_close"] = '</li>';
		$config["next_tag_open"] = '<li>';
		$config["next_tag_close"] = '</li>';
		$config["prev_tag_open"] = "<li>";
		$config["prev_tag_close"] = "</li>";
		$config["cur_tag_open"] = "<li class='active'><a href='#'>";
		$config["cur_tag_close"] = "</a></li>";
		$config["num_tag_open"] = "<li>";
		$config["num_tag_close"] = "</li>";
		$config["num_links"] = 1;
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}

	public function ajax_pagination()
	{
		$query=$this->input->post('query');
		$this->layout = '';
		$offset = $this->uri->segment(4,0);
		$limit=10;
		$start = ($offset - 1) * $limit;
		$page_url='admin/Tour/showalltour';
		$total_rows=$this->Tour_model->get_total();
		$pagination=$this->pagination($limit,$page_url,$total_rows);
		if($query=='lower')
		{
			$tour=$this->Tour_model->get_data_with_limit($limit,$start,$query);
		}
		elseif($query=='higher')
		{
			$tour=$this->Tour_model->get_data_with_limit($limit,$start,'',$query);
		}
		else
		{
			$tour=$this->Tour_model->get_data_with_limit($limit,$start);
		}
		$data = array();
		$data['pagination'] = $pagination;
		$data['tour'] = $tour;
		echo json_encode($data);exit();
	}


	public function single_tour()
	{


		$this->layout = 'admin/user_dashborad';
		$this->load->view('tours/single_tour');


	}



	public function get_specific_tour($id)
	{

		$this->layout = 'admin/user_dashborad';
		$data['data']=$this->Tour_model->get_by('id',$id);
		$data['id'] = $id;
		$this->load->view('tours/single_tour',$data);
	} 
	public function showalltour()
	{

		$data=array();
		$this->layout = 'admin/user_dashborad';
		$data['key']=$this->Tour_model->select_all();
		$data['visa']=$this->Visa_model->select_all();
		$this->load->view('tours/alltour',$data);
	}


	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{

			$data=$this->Subscriber_model->delete_by('id',$id);
			if($data)
			{
				$this->session->set_flashdata('delete','mymsg');
				redirect('admin/Subscriber/show_subscriber');
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			$data['data']=$this->News_model->get_by('id',$id);
			$this->load->view('news/edit',$data);
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function get_value_by_ajax()
	{


		$this->layout = '';
		$id=$this->input->post('value');
		$data=$this->Tour_model->get_by('tour_to',$id);
		echo json_encode($data);

	}
	public function get_value_by_tour()
	{
		$this->layout = '';
		$value=$this->input->post('search');
		if(!empty($value))
			$countries=$this->Tour_model->get_by_like('tour_to',$value);
		else
			{ $countries=$this->Tour_model->get_all_data();

			}

			$data = array();
			foreach ($countries as $country) {
				$data[]  = array('value'=>$country['tour_to'],'label'=>$country['tour_to']);
			}
			echo json_encode($data);
			exit;
		}
		
		
		public function update()
		{
			if($this->session->userdata('user_email'))
			{
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$id=$this->input->post('id');
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				$temp_img= $this->input->post('temp_img');
				$data = array(
					'news_heading' =>$this->input->post('news_heading'),
					'news_description' =>$this->input->post('department_desc'),
					'news_date' =>$this->input->post('news_date'),
					'image' =>!empty($image)?$image:$temp_img

				);
				$data=$this->News_model->update_by('id',$id,$data);
				redirect('admin/News/show_news');	
				
				
			}
			else
			{
				redirect('admin/Login/loginload');
			}



		}
	}