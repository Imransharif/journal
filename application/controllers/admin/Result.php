
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Result_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Teacher')
			{
				$this->load->view('result/add_result');
			}

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_events()
	{
		$this->layout = 'admin/user_dashboard';
		$data['events']=$this->Events_model->get_where('*','','', true, '', '', '');
		$this->load->view('events/show_user_side_events',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}

	public function download($file)
	{	
		$file=str_replace("%20", ' ', $file);
		redirect("images/$file");
	}

	public function show_result()
	{
		
		if($this->session->userdata('user_email'))
		{   

			if($this->session->userdata('status')=='Director_Qec' || $this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Manager' || $this->session->userdata('status')=='Cordinator' || $this->session->userdata('status')=='Teacher' || $this->session->userdata('status')=='Student')
			{

				$data['result']=$this->Result_model->get_where('*','',true, 'id DESC', '','');
				$this->load->view('result/show_result',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Teacher')
			{
				if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4|pdf|doc|docx|rtf|text|txt|pptx';
					$this->load->library('upload',$config);
					$this->upload->do_upload('file');
					$data = $this->upload->data();
					if($data) 
					{
						$file = $data['file_name'];
						$file2=str_replace(" ", '_', $file); 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				$nophoto = "nofile.png";
				$data = array(
					'course_name' =>$this->input->post('course_name'),
					'file' =>!empty($file2)?$file2:$nophoto,
				);
				$data1=$this->Result_model->save($data);
				redirect('admin/Result/show_result');	
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{ 
			if($this->session->userdata('status')=='Teacher')
			{
				$data=$this->Result_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Result/show_result');	
				}
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Teacher')
			{
				$data['data']=$this->Result_model->get_by('id',$id);
				$this->load->view('result/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}



	public function update()
	{

		if($this->session->userdata('user_email'))
		{
			$id=$this->input->post('id');
			if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != '') 
			{
				$config= array();
				$config['upload_path'] = FCPATH.'images/';
				$config['allowed_types'] = 'gif|jpg|png|mp4|pdf|doc|docx|rtf|text|txt|pptx';
				$this->load->library('upload',$config);
				$this->upload->do_upload('file');
				$data = $this->upload->data();
				if($data) 
				{
					$file = $data['file_name'];
					$file2=str_replace(" ", '_', $file); 

				}
				else
				{
					echo $this->upload->display_errors();
				}
			}
			
			$tmp_file =$this->input->post('tmp_file');
			$data = array(
				'course_name' =>$this->input->post('course_name'),
				'file' =>!empty($file2)?$file2:$tmp_file,
			);
			$data=$this->Result_model->update_by('id',$id,$data);
			redirect('admin/Result/show_result');	
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}
}