
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Manager_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Department_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Director_Qec')
			{
				$this->load->view('manager/addmanager');
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_programe($user_id='')
	{
		$data=array();
		$this->layout = 'admin/user_dashboard';
		if($user_id==1)
		{
			$data['programe']='Graduate';
		}
		if($user_id==2)
		{
			$data['programe']='Undergraduate';
		}
		if($user_id==3)
		{
			$data['programe']='Postgraduate';
		}
		
		$where = (!empty($user_id))?"programe_id = '".$user_id."' ":"programe_id > 0 ";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'left'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);

		$this->load->view('programe/show_user_side_programe',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}


	public function show_manager()
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Manager')
			{
				$where = "email = '".$this->session->userdata('user_email')."'";
				$data['key']=$this->Manager_model->get_where('*',$where,'', true, '', '', '');
				$this->load->view('manager/show_manager',$data);
			}
			if($this->session->userdata('status')=='Director_Qec' || $this->session->userdata('status')=='Director_Pb')
			{

				$data['key']=$this->Manager_model->get_where('*','','', true, '', '', '');
				$this->load->view('manager/show_manager',$data);
			}
		}

		else
		{
			redirect('admin/Login/loginload');
		}

		
	}

	public function insertion()
	{
		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Director_Qec')
			{
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				$nophoto = "nophoto.png";
				$data = array(
					'name' =>$this->input->post('name'),
					'image' =>!empty($image)?$image:$nophoto,
					'phone' =>$this->input->post('phone'),
					'email' =>$this->input->post('email'),
					'password' =>$this->input->post('password'),
					'status' =>'Manager',
					'address' =>$this->input->post('address')

				);
				$data1=$this->Manager_model->save($data);
				redirect('admin/Manager/show_manager');	
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}


	}





	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Director_Qec')
			{
				$data=$this->Manager_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Manager/show_manager');	
				}
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Director_Qec')
			{
				$data['data']=$this->Manager_model->get_by('id',$id);
				$this->load->view('manager/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}




	public function update()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Pb' || $this->session->userdata('status')=='Director_Qec')
			{
				$id=$this->input->post('id');
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}

				$tmp_image= $this->input->post('tmp_image');
				$data = array(
					'name' =>$this->input->post('name'),
					'image' =>!empty($image)?$image:$tmp_image,
					'phone' =>$this->input->post('phone'),
					'email' =>$this->input->post('email'),
					'password' =>$this->input->post('password'),
					'status' =>'Manager',
					'address' =>$this->input->post('address')

				);
				$data=$this->Manager_model->update_by('id',$id,$data);
				redirect('admin/Manager/show_manager');
			}
		}
		else
		{
			redirect('admin/Login/loginload');
		}



	}


}
