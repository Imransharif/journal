
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_info extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Article_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Add_info_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			
				$this->load->view('company_info/add_info');

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	public function showcategory()
	{

		$this->load->view('category/showcategory');	
	}

	public function show_user_side_news()
	{
		
		if($this->session->userdata('user_email'))
		{    


			if($this->session->userdata('status')=='Director_Qec')
			{

		$data['company_info']=$this->Article_model->get_where('*','',true, 'id DESC', '', '');
				$this->load->view('article/show',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{
		
			$data = array(
				'heading' =>$this->input->post('news_heading'),
				'description' =>$this->input->post('department_desc'),

			);
			$data1=$this->Article_model->save($data);
			redirect('admin/Add_info/show_article');	

	}




	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Add_info_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Add_info/show_info');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}




	}