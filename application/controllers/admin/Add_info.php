
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_info extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Add_info_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			
				$this->load->view('company_info/add_info');

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_news()
	{
		$this->layout = 'admin/user_dashboard';
		$data['news']=$this->Add_info_model->get_where('*','','', true, '', '', '');
		$this->load->view('news/user_side_news',$data);

	}
	public function show_user_side_single_news($id)
	{

		$this->layout = 'admin/user_dashboard';
		$where = (!empty($id))?"id = '".$id."' ":"id > 0 ";
		$data['remaining_news']=$this->Add_info_model->not_equal($id);
		$data['single_news']=$this->Add_info_model->get_where('*',$where,'', true, '', '', '');
		$this->load->view('news/single_news',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}


	public function show_info()
	{
		
		if($this->session->userdata('user_email'))
		{    


			if($this->session->userdata('status')=='Director_Qec')
			{

				$data['company_info']=$this->Add_info_model->get_where('*','',true, 'id DESC', '', '');
				$this->load->view('company_info/show_info',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{

		if($this->session->userdata('user_email'))
		{

			$data = array(
				'heading' =>$this->input->post('news_heading'),
				'description' =>$this->input->post('department_desc'),

			);
			$data1=$this->Add_info_model->save($data);
			redirect('admin/Add_info/show_info');	
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}




	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Add_info_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Add_info/show_info');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['data']=$this->Add_info_model->get_by('id',$id);
				$this->load->view('company_info/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}



		
		
		public function update()
		{
			$id=$this->input->post('id');
			$description1=$this->input->post('description1');
			// if($this->session->userdata('user_email'))
			// {
			// 	if($this->session->userdata('status')=='Director_Qec' )
			// 	{
		
					$data = array(
						'heading' =>$this->input->post('heading'),
						'description' =>!empty($this->input->post('department_desc'))?$this->input->post('department_desc'):$description1,

					);
					$data=$this->Add_info_model->update_by('id',$id,$data);
					redirect('admin/Add_info/show_info');	


			// 	}
			// }
			// else
			// {
			// 	redirect('admin/Login/loginload');
			// }



		}
	}