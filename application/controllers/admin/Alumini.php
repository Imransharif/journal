
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumini extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Director_pb_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Alumini_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('alumini/add_alumini');
			}

		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_alumin()
	{
		$this->layout = 'admin/user_dashboard';
		$data['alumini']=$this->Alumini_model->get_where('*','',true, 'id DESC', '','');
		$this->load->view('alumini/show_user_side_alumani',$data);

	}

	public function showcategory()
	{

		$this->load->view('category/showcategory');
		
	}


	public function show_alumin()
	{
		
		if($this->session->userdata('user_email'))
		{   

			if($this->session->userdata('status')=='Director_Qec' )
			{

				$data['alumini']=$this->Alumini_model->get_where('*','',true, 'id DESC', '','');
				$this->load->view('alumini/show_alumini',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}

	public function insertion()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}


				$nophoto = "nophoto.png";
				$data = array(
					'name' =>$this->input->post('name'),
					'email' =>$this->input->post('email'),
					'phone' =>$this->input->post('phone'),
					'programe' =>$this->input->post('programe'),
					'cname' =>$this->input->post('cname'),
					'caddress' =>$this->input->post('caddress'),
					'address' =>$this->input->post('address'),
					'image' =>!empty($image)?$image:$nophoto

				);
				$data1=$this->Alumini_model->save($data);
				redirect('admin/Alumini/show_alumin');	
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}


	}



	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Alumini_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Alumini/show_alumin');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}


	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			$data['data']=$this->Alumini_model->get_by('id',$id);
			$this->load->view('alumini/edit',$data);
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}

	
	public function update()
	{

		if($this->session->userdata('user_email'))
		{
			$id=$this->input->post('id');
			if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
			{
				$config= array();
				$config['upload_path'] = FCPATH.'images/';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('photo');
				$data = $this->upload->data();
				if($data) 
				{
					$image = $data['file_name']; 

				}
				else
				{
					echo $this->upload->display_errors();
				}
			}


			$tmp_name= $this->input->post('tmp_name');
			$data = array(
				'name' =>$this->input->post('name'),
				'email' =>$this->input->post('email'),
				'phone' =>$this->input->post('phone'),
				'programe' =>$this->input->post('programe'),
				'cname' =>$this->input->post('cname'),
				'caddress' =>$this->input->post('caddress'),
				'address' =>$this->input->post('address'),
				'image' =>!empty($image)?$image:$tmp_name

			);
			$data=$this->Alumini_model->update_by('id',$id,$data);
			redirect('admin/Alumini/show_alumin');	
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

	}
}