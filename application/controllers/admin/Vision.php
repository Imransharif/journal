
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vision extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Teacher_model');
		$this->load->model('Vision_mission_model');
		$this->load->model('Department_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('vision_mission/add_vission_mission');
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_vission()
	{
		$data=array();
		$this->layout = 'admin/user_dashboard';
		if($user_id==1)
		{
			$data['programe']='Graduate';
		}
		if($user_id==2)
		{
			$data['programe']='Undergraduate';
		}
		if($user_id==3)
		{
			$data['programe']='Postgraduate';
		}
		
		$where = (!empty($user_id))?"programe_id = '".$user_id."' ":"programe_id > 0 ";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'left'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);

		$this->load->view('programe/show_user_side_programe',$data);

	}




	public function show_vision()
	{
		
		if($this->session->userdata('user_email'))
		{
			
			if($this->session->userdata('status')=='Director_Qec')
			{

				$data['key']=$this->Vision_mission_model->get_where('*','', true, '', '', '');
				$this->load->view('vision_mission/show_vision_mission',$data);
			}
		}

		else
		{
			redirect('admin/Login/loginload');
		}

		
	}

	public function insertion()
	{


		if($this->session->userdata('user_email'))
		{		
			$data = array(
				'vision' =>$this->input->post('vision'),

			);
			$data1=$this->Vision_mission_model->save($data);
			redirect('admin/Vision/show_vision');		
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		

	}



	public function home()
	{
		if($this->session->userdata('user_email'))
		{

			$this->load->view('home/home');
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

		
	}



	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{

			$data['data']=$this->Vision_mission_model->get_by('id',$id);
			$this->load->view('vision_mission/edit',$data);
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	


		public function update()
		{
			if($this->session->userdata('user_email'))
			{

				$id=$this->input->post('id');
				$data = array(
					'vision' =>$this->input->post('department_desc'),
				);
				$data=$this->Vision_mission_model->update_by('id',$id,$data);
				if($data)
				{
					$this->session->set_flashdata('update','mymsg');
					redirect('admin/Vision/show_vision'); 
				}	
			} 
			else
			{
				redirect('admin/Login/loginload');
			}
			

		}

	}
