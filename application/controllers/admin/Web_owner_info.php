
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_owner_info extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Category_model');
		$this->load->model('Web_owner_info_model');
		$this->load->model('Department_model');
		
	}

	public function index()
	{

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('web_owner_info/web_owner_info');
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function show_user_side_programe($user_id='')
	{
		$data=array();
		$this->layout = 'admin/user_dashboard';
		if($user_id==1)
		{
			$data['programe']='Graduate';
		}
		if($user_id==2)
		{
			$data['programe']='Undergraduate';
		}
		if($user_id==3)
		{
			$data['programe']='Postgraduate';
		}
		
		$where = (!empty($user_id))?"programe_id = '".$user_id."' ":"programe_id > 0 ";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'inner'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);

		$this->load->view('programe/show_user_side_programe',$data);

	}

	public function show_web_owner_info()
	{
		if($this->session->userdata('user_email'))
		{ 
			if($this->session->userdata('status')=='Director_Qec' )
			{

				$data['key']=$this->Web_owner_info_model->get_where('*','', true, '', '', '');

				$this->load->view('web_owner_info/show_web_owner_info',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}


	public function showprograme()
	{
		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['key']=$this->Category_model->get_where('*','', true, '', '', '');
				$this->load->view('programe/showprograme',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}

		
	}
	public function show_web_owner_profile()
	{ 
		$data = array();
		$this->layout = 'admin/user_dashboard';
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'inner'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['single_department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins,'', '','', '', '', '','', '', '',true);
		$data['key']=$this->Web_owner_info_model->get_where('*','', true, '', '', '');
		$this->load->view('web_owner_info/show_web_owner_profile',$data);
		
	}

	public function insertion()
	{
		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
				{
					$config= array();
					$config['upload_path'] = FCPATH.'images/';
					$config['allowed_types'] = 'gif|jpg|png|mp4';
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$data = $this->upload->data();
					if($data) 
					{
						$image = $data['file_name']; 

					}
					else
					{
						echo $this->upload->display_errors();
					}
				}


				$nophoto = "nophoto.png";
				$data = array(
					'name' =>$this->input->post('name'),
					'logo' =>!empty($image)?$image:$nophoto,
					'details' =>$this->input->post('description'),
					'phone' =>$this->input->post('phone'),
					'email' =>$this->input->post('email'),
					'facebook' =>$this->input->post('facebook'),
					'instagram' =>$this->input->post('instagram'),
					'twitter' =>$this->input->post('twitter'),
					'g_plus' =>$this->input->post('g_plus'),
					'viemo' =>$this->input->post('viemo'),
					'linkden' =>$this->input->post('linkden'),
					'password' =>$this->input->post('password'),
					'status' =>'Director_Qec',
					'adress' =>$this->input->post('address')

				);
				$data1=$this->Web_owner_info_model->save($data);	
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}
		

	}


	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Web_owner_info_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Tour/showtour');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{

				$data['data']=$this->Web_owner_info_model->get_by('id',$id);
				$this->load->view('web_owner_info/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	public function view($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['data']=$this->Web_owner_info_model->get_by('id',$id);
				$this->load->view('web_owner_info/view',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}
	
	

	public function update()
	{
		if($this->session->userdata('user_email'))
		{
			$id=$this->input->post('id');
			if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') 
			{
				$config= array();
				$config['upload_path'] = FCPATH.'images/';
				$config['allowed_types'] = 'gif|jpg|png|mp4';
				$this->load->library('upload',$config);
				$this->upload->do_upload('photo');
				$data = $this->upload->data();
				if($data) 
				{
					$image = $data['file_name']; 

				}
				else
				{
					echo $this->upload->display_errors();
				}
			}
			
			$tmp_image= $this->input->post('tmp_image');
			$data = array(
				'name' =>$this->input->post('name'),
				'logo' =>!empty($image)?$image:$tmp_image,
				'details' =>$this->input->post('department_desc'),
				'phone' =>$this->input->post('phone'),
				'email' =>$this->input->post('email'),
				'facebook' =>$this->input->post('facebook'),
				'instagram' =>$this->input->post('instagram'),
				'twitter' =>$this->input->post('twitter'),
				'g_plus' =>$this->input->post('g_plus'),
				'viemo' =>$this->input->post('viemo'),
				'linkden' =>$this->input->post('linkden'),
				'password' =>$this->input->post('password'),
				'status' =>'Director_Qec',
				'adress' =>$this->input->post('address')

			);
			$data=$this->Web_owner_info_model->update_by('id',$id,$data);
			if($data)
			{
				$this->session->set_flashdata('update','mymsg');
				redirect('admin/Web_owner_info/show_web_owner_info'); 
			}	
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
	}

}
