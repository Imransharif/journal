
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Category_model');
		$this->load->model('Programe_model');
		
		
	}

	public function index()
	{

		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$this->load->view('category/addcategory');
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}


	public function show_category()
	{
		
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['key']=$this->Category_model->get_where('*','', true, '', '', '');
				$this->load->view('category/showcategory',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}

	public function ajax_value()
	{
		$data=array();
		$id=$this->input->post('value');
		$where = "p_id = '".$id."'";
		if($this->session->userdata('user_email'))
		{
			$data=$this->Category_model->get_where('*',$where, true, '', '', '');
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		echo json_encode($data);exit();

		
	}

	public function insertion()
	{
		

		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{

				$data = array(
					'category_name' =>$this->input->post('subcategory_name'),
					'p_id' =>$this->input->post('programe_name'),


				);
				$data1=$this->Category_model->save($data);
				if($data1)	
				{
					redirect('admin/Category/show_category');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}
		

	}




	public function delete($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data=$this->Category_model->delete_by('id',$id);
				if($data)
				{
					$this->session->set_flashdata('delete','mymsg');
					redirect('admin/Category/show_category');
				}
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}

	}
	public function edit($id)
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$data['data']=$this->Category_model->get_by('id',$id);
				$this->load->view('category/edit',$data);
			}
		} 
		else
		{
			redirect('admin/Login/loginload');
		}
		
	}


	public function update()
	{
		if($this->session->userdata('user_email'))
		{
			if($this->session->userdata('status')=='Director_Qec' )
			{
				$id=$this->input->post('id');
				$thumbnailimage= $this->input->post('thumbnailimage');
				$data = array(
					'category_name' =>$this->input->post('subcategory_name'),
					'p_id' =>$this->input->post('programe_name'),

				);
				$data=$this->Category_model->update_by('id',$id,$data);
				if($data)
				{
					$this->session->set_flashdata('update','mymsg');
					redirect('admin/Category/show_category'); 
				}	
			} 
		}
		else
		{
			redirect('admin/Login/loginload');
		}


	}


}
