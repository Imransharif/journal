
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programe extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'admin/admin_dashborad';
		$this->load->model('Category_model');
		$this->load->model('Programe_model');
		$this->load->model('Department_model');
		
	}

	public function index()
	{
		$data['category']=$this->Category_model->get_where('*','', true, '', '', '');
		$data['subcategory']=$this->Category_model->get_where('*','', true, '', '', '');
		// if($this->session->userdata('user_email'))
		// {
		$this->load->view('programe/addprograme',$data);
		// } 
		// else
		// {
		// 	redirect('admin/Login/loginload');
		// }
		
	}

	public function show_user_side_programe($user_id='')
	{
		$data=array();
		$this->layout = 'admin/user_dashboard';
		if($user_id==1)
		{
			$data['programe']='Graduate';
		}
		if($user_id==2)
		{
			$data['programe']='Undergraduate';
		}
		if($user_id==3)
		{
			$data['programe']='Postgraduate';
		}
		
		$where = (!empty($user_id))?"programe_id = '".$user_id."' ":"programe_id > 0 ";
		$joins = array(
			'0' => array('table_name' => 'category category',
				'join_on' => ' category.id = department.category_id ',
				'join_type' => 'inner'
			)
		);
		$from_table = "department department";
		$select_from_table = 'department.*,category.*';
		$data['department'] = $this->Department_model->get_by_join($select_from_table, $from_table, $joins, $where, '','', '', '', '','', '', '',true);

		$this->load->view('programe/show_user_side_programe',$data);
			
	}

	public function show_programe()
	{

		$this->load->view('programe/show_programe');
		
	}


	public function showprograme()
	{
		
		// if($this->session->userdata('user_email'))
		// {
		$data['key']=$this->Category_model->get_where('*','', true, '', '', '');
		$this->load->view('programe/showprograme',$data);
		// } 
		// else
		// {
			// redirect('admin/Login/loginload');
		// }

		
	}

	public function insertion()
	{
		

			// if($this->session->userdata('user_email'))
			// {

		$data = array(
			'category_id' =>$this->input->post('category_name'),
			'subcategory_id' =>$this->input->post('subcategory_name'),
			'programe_name' =>$this->input->post('programe_name'),
		);
		$data1=$this->Programe_model->save($data);	
			// } 
			// else
			// {
			// 	redirect('admin/Login/loginload');
			// }
		

	}






	}
