<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Add_info/insertion" method="POST" enctype="multipart/form-data">
          <div class="form-group row">
           <label for="input-17" class="col-sm-2 col-form-label">Enter   Heading</label>
           <div class="col-sm-10">
            <input type="text" name="news_heading" class="form-control form-control-square" id="input-17"  placeholder="Enter News  Heading">
          </div>
        </div>
        <div class="department_desc">
        <div class="form-group row ">
         <label for="input-17" class="col-sm-2 col-form-label ">Enter   Description</label>
         <div class="col-sm-10">
          <textarear class="summernote"></textarear>
           </div>
          </div>
          </div>
   
      <div id="n_cl">

      </div>
     

     <div class="form-group">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

  </form>

</div>
</div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
<script type="text/javascript">

  $(document).on('change','#category_name',function() 
  {
      var temp = '';
      var value=$('#category_name').val();
      $.ajax
      ({
        url:'<?php echo base_url('admin/Subcategory/ajax_value')?>',
        method:'POST',
        data:{value:value},
        dataType:"json",
        success:function(data)
        {
          data.forEach((val,ind)=>{
            temp += `<option>${val.name}</option>`;
          });
          $(".appenddata").html(temp);
          console.log(temp);
         
        }
      });
  })
</script>