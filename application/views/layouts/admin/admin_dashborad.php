<!DOCTYPE html>
<html lang="en">
<?php $Web_owner_info_model=Web_owner_info_model() ?>
<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 08:43:00 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <base href="<?php echo base_url()?>">
  <script>
    var base_url = '<?php echo base_url();?>';
  </script>
  <script src="assets/js/jquery.min.js"></script>

  <title>Welcome to | <?php echo $Web_owner_info_model[0]['name'] ?></title>
  <!--favicon-->
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
  <!-- notifications css -->
  <link rel="stylesheet" href="assets/plugins/notifications/css/lobibox.min.css"/>
  <!-- Vector CSS -->
  <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
  <!-- simplebar CSS-->
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="assets/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="assets/css/app-style.css" rel="stylesheet"/>
  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <script src="<?php echo base_url('assets/dist/summernote-bs4.min.js')?>"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/summernote-bs4.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dist/summernote.css')?>">
  <style>
    .footer {
      position: absolute!important;
      bottom: !important;
    }
  </style>
</head>
<body>
  <!-- Start wrapper-->
  <div id="wrapper">
    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="admin/Login/deshboard">
       <img src="assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
       <h5 class="logo-text">Dashboard</h5>
     </a>
   </div>
   <ul class="sidebar-menu do-nicescrol">
     <!--  <li class="sidebar-header">MAIN NAVIGATION</li> -->

     <?php  
     if($this->session->userdata('status')=='Director_Qec')
     {
      ?>
   
      <li>
        <a href="index.html" class="waves-effect">
          <i class="icon-home"></i><span>Home</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="admin/Add_info/show_info"><i class="fa fa-long-arrow-right"></i> Advertise</a></li>
        </ul>
      </li>
      <li>
        <a href="admin/Department/show_department" class="waves-effect">
          <i class="icon-home"></i><span>About us</span><i class="fa fa-angle-left pull-right"></i> 
        </a>
        <ul class="sidebar-submenu">
          <li><a href="admin/add_info/show_info"><i class="fa fa-long-arrow-right"></i> Company info</a></li>
        </ul>
      </li>
      <li>
        <a href="admin/Department/show_department" class="waves-effect">
          <i class="icon-home"></i><span>Archieve</span><i class="fa fa-angle-left pull-right"></i> 
        </a>
        <ul class="sidebar-submenu">
          <li><a href="index.html"><i class="fa fa-long-arrow-right"></i>Article</a></li>
        </ul>
      </li>
      <li>
      <a href="admin/Coordinator/show_coordinator" class="waves-effect">
        <i class="icon-handbag"></i><span>Team</span> 
      </a>
    </li>
      <li>
        <a href="admin/Events/show_events" class="waves-effect">
          <i class="icon-handbag"></i><span>Events</span> 
        </a>
      </li>
      <li>
        <a href="admin/News/show_news" class="waves-effect">
          <i class="icon-handbag"></i><span>News</span> 
        </a>
      </li>
        <li>
        <a href="admin/Subscriber/show_subscriber" class="waves-effect">
          <i class="icon-handbag"></i><span>Blog</span> 
        </a>
      </li>
       <li>
        <a href="admin/Department/show_department" class="waves-effect">
          <i class="icon-home"></i><span>Submissions</span><i class="fa fa-angle-left pull-right"></i> 
        </a>
        <ul class="sidebar-submenu">
          <li><a href="index.html"><i class="fa fa-long-arrow-right"></i>All Submission</a></li>
        </ul>
      </li>
      <li>
        <a href="admin/Department/show_department" class="waves-effect">
          <i class="icon-home"></i><span>Membership</span><i class="fa fa-angle-left pull-right"></i> 
        </a>
        <ul class="sidebar-submenu">
          <li><a href="index.html"><i class="fa fa-long-arrow-right"></i>All Member</a></li>
        </ul>
      </li>
      <li>
        <a href="admin/Subscriber/show_subscriber" class="waves-effect">
          <i class="icon-handbag"></i><span>Contact us</span> 
        </a>
      </li>
      <?php 
    }
    ?>

    <?php 
    if($this->session->userdata('status')=='Director_Qec')
    {
      ?>
      <li>
        <a href="admin/Slider/show_slider" class="waves-effect">
          <i class="icon-handbag"></i><span>Slider</span> 
        </a>
      </li>
      <?php 
    }
?>
</ul>

</div>
<!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top gradient-ibiza">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
   </li>
</ul>

<ul class="navbar-nav align-items-center right-nav-link">

<li class="nav-item">
  <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
    <span class="user-profile"><img src="images/<?php echo  $this->session->userdata('logo') ?>" class="img-circle" alt="user avatar">
    </span>
  </a>
  <ul class="dropdown-menu dropdown-menu-right animated fadeIn">
   <li class="dropdown-item user-details">
    <a href="javaScript:void();">
     <div class="media">
       <div class="avatar"><img class="align-self-start mr-3" src="images/<?php echo  $this->session->userdata('logo') ?>" alt="user avatar"></div>
       <div class="media-body">
        <h6 class="mt-2 user-title"><?php echo  $this->session->userdata('name') ?></h6>
        <p class="user-subtitle"><?php echo  $this->session->userdata('email') ?></p>
      </div>
    </div>
  </a>
</li>
<a href="admin/login/logout">
  <li class="dropdown-item"><i class="icon-power mr-2"></i> Logout</li>
</a>
</ul>
</li>
</ul>
</nav>
</header>
<!--End topbar header-->
{_yield}
<div class="clearfix"></div>

<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->

<!--Start footer-->
<footer class="footer">
  <div class="container">
    <div class="text-center">
      Copyright © 2018 <?php echo $Web_owner_info_model[0]['name'] ?>
    </div>
  </div>
</footer>
<!--End footer-->

</div><!--End wrapper-->

<!-- Bootstrap core JavaScript-->

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>  
<!-- Vector map JavaScript -->
<?php // array('heads'=>'head1_|_head2','paras'=>'paraa1_|_para2'); ?>
<script src="assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline JS -->
<script src="assets/plugins/sparkline-charts/jquery.sparkline.min.js"></script>
<!-- Chart js -->
<script src="assets/plugins/Chart.js/Chart.min.js"></script>
<!--notification js -->
<script src="assets/plugins/notifications/js/lobibox.min.js"></script>
<script src="assets/plugins/notifications/js/notifications.min.js"></script>
<!-- Index js -->
<script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
<script type="text/javascript">
  $('.summernote').summernote({
    tabsize: 2,
    height: 150
  });
  $('.add_clone').click(function(){
   addContent();
 });
  var content_row = 1;
  function addContent(){
    var html='';
    html += '<div class="form-group row"><label for="input-17" class="col-sm-2 col-form-label ">Enter Program List Point Name</label><div class="col-sm-10"><input type="text" name="p_point[]" class="form-control form-control-square" id="input-17"  placeholder="Enter Program List Point Name"></div></div><div class="form-group row"><label for="input-17" class="col-sm-2 col-form-label">Enter Program Point Description</label><div class="col-sm-10">';
    html += '<textarea class="form-control nw__nw" id="code_preview' + content_row + '" name="p_p_desc[]" style="height: 300px;"></textarea>';
    html += '</div>';
    html += '</div>';
    $('#n_cl').append(html);
    $('#code_preview' + content_row).summernote({height: 150});
    content_row++;
    $('.nw__nw').attr('name','p_p_desc[]');
  }
  $(document).ready(function(){
    $('.note-popover').hide();
    $('.note-editable').on('hover blur',function(){
      $(this).prev().html($(this).html());
    });
    $('.note-editable').prev().attr('name','p_p_desc[]');
    $('.department_desc .note-editable').prev().attr('name','department_desc');
  });

</script>  
</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 08:43:00 GMT -->
</html>
