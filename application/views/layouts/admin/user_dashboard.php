<!DOCTYPE html>
<html>
<?php $Web_owner_info_model=Web_owner_info_model() ?>
<!-- Mirrored from glorytheme.com/template/universh/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 08:18:16 GMT -->
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <title>Welcome to | <?php echo $Web_owner_info_model[0]['name'] ?></title>
    <meta name="keywords" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template" />
    <meta name="description" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template">
    <meta name="author" content="glorytheme.com">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<?php echo base_url()?>">
    <script>
        var base_url = '<?php echo base_url();?>';
    </script>
    <script src="assets1/js/lib/jquery.js"></script>
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets1/images/default/favicon.png">
    <!-- Web Fonts  -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
    rel='stylesheet' type='text/css'>

    <!-- Lib CSS -->
    <link rel="stylesheet" href="assets1/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="assets1/css/lib/animate.min.css">
    <link rel="stylesheet" href="assets1/css/lib/font-awesome.min.css">
    <link rel="stylesheet" href="assets1/css/lib/univershicon.css">
    <link rel="stylesheet" href="assets1/css/lib/owl.carousel.css">
    <link rel="stylesheet" href="assets1/css/lib/prettyPhoto.css">
    <link rel="stylesheet" href="assets1/css/lib/menu.css">
    <link rel="stylesheet" href="assets1/css/lib/timeline.css">
    
    <!-- Revolution CSS -->
    <link rel="stylesheet" href="assets1/revolution/css/settings.css">
    <link rel="stylesheet" href="assets1/revolution/css/layers.css">
    <link rel="stylesheet" href="assets1/revolution/css/navigation.css"> 
    
    <!-- Theme CSS -->
    <link rel="stylesheet" href="assets1/css/theme.css">
    <link rel="stylesheet" href="assets1/css/theme-responsive.css">
    
        <!--[if IE]>
            <link rel="stylesheet" href="css/ie.css">
        <![endif]-->
        
        <!-- Head Libs -->
        <script src="assets1/js/lib/modernizr.js"></script>
        
        <!-- Skins CSS -->
        <link rel="stylesheet" href="assets1/css/skins/default.css">
        
        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="assets1/css/custom.css">
    </head>
    <body>

        <!-- Page Loader -->
        <div id="pageloader">
            <div class="loader-inner">
                <img src="assets1/images/default/preloader.gif" alt="">
            </div>
        </div><!-- Page Loader -->
        
        <!-- Back to top -->
        <a href="#0" class="cd-top">Top</a>
        <!-- Header Begins -->  
        <header id="header" class="default-header colored flat-menu">
            <div class="header-top">
                <div class="container">
                    <nav>
                        <ul class="nav nav-pills nav-top">
                            <li class="phone">
                                <span><i class="fa fa-envelope"></i><?php echo $Web_owner_info_model[0]['email'] ?></span>                    </li>
                                <li class="phone">
                                    <span><i class="fa fa-phone"></i><?php echo $Web_owner_info_model[0]['phone'] ?></span>                  </li>
                                </ul>
                            </nav>
                            <ul class="social-icons color">
                                <li class="googleplus"><a title="googleplus" target="_blank" href="<?php echo $Web_owner_info_model[0]['g_plus'] ?>">googleplus</a></li>
                                <li class="facebook"><a href="<?php echo $Web_owner_info_model[0]['email'] ?>" target="_blank" title="Facebook">Facebook</a></li>
                                <li class="twitter"><a href="<?php echo $Web_owner_info_model[0]['twitter'] ?>" target="_blank" title="Twitter">Twitter</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="container">
                        <div class="logo">
                            <a href="Home">
                             <img alt="Universh" width="211" height="40" data-sticky-width="150" data-sticky-height="28" src="images/<?php echo $Web_owner_info_model[0]['logo'] ?>">         </a>        </div>
                             <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
                                <i class="fa fa-bars"></i>      </button>
                            </div>
                            <div class="navbar-collapse nav-main-collapse collapse">
                                <div class="container">
                                    <nav class="nav-main mega-menu">
                                        <ul class="nav nav-pills nav-main" id="mainMenu">
                                           <li class="nav-item  <?php echo (activate_sub_menu('home/index') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('home/index');?>" class="nav-link ">
                                                <span class="title">Home</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  <?php echo (activate_sub_menu('Web_owner_info/show_web_owner_profile') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('admin/Web_owner_info/show_web_owner_profile');?>" class="nav-link ">
                                                <span class="title">Qec</span>
                                            </a>
                                        </li>

                                        <li class="nav-item  <?php echo (activate_sub_menu('Programe/show_user_side_programe/3') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('admin/Programe/show_user_side_programe/3');?>" class="nav-link ">
                                                <span class="title">Pb</span>
                                            </a>
                                        </li>

                                        <li class="nav-item  <?php echo (activate_sub_menu('Alumini/show_user_side_alumin') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('admin/Alumini/show_user_side_alumin');?>" class="nav-link ">
                                                <span class="title">Alumini</span>
                                            </a>
                                        </li>

                                          <li class="nav-item   <?php echo (activate_sub_menu('Events/show_user_side_events') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('admin/Events/show_user_side_events');?>" class="nav-link ">
                                                <span class="title">Events</span>
                                            </a>
                                        </li>

                                         <li class="nav-item  <?php echo (activate_sub_menu('News/show_user_side_news') != '' )?'active':'';?>  ">
                                            <a href="<?php echo base_url('admin/News/show_user_side_news');?>" class="nav-link ">
                                                <span class="title">News</span>
                                            </a>
                                        </li>

                                        <li class="dropdown  <?php echo (activate_menu('Programe') != '' )?'active':'';?>" >
                                            <a class="dropdown-toggle" href="javascript:;">
                                                Programe
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                
                                                <li class="<?php echo (activate_sub_menu('Programe/show_user_side_programe') != '' )?'active':'';?> ">
                                                    <a href="admin/Programe/show_user_side_programe/1">Graduate</a>
                                                </li>
                                                
                                                <li class="<?php echo (activate_sub_menu('Programe/show_user_side_programe') != '' )?'active':'';?> "><a href="admin/Programe/show_user_side_programe/2">Undergraduate</a></li>
                                                
                                                <li class="<?php echo (activate_sub_menu('Programe/show_user_side_programe') != '' )?'active':'';?> "><a href="admin/Programe/show_user_side_programe/3">PostGraduate</a></li>
                                            </ul>
                                        </li>


                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </header><!-- Header Ends -->

                    {_yield}

                    <!-- Footer -->
                    <footer id="footer" class="footer-1">
                        <div class="main-footer widgets-dark typo-light">
                            <div class="container">
                                <div class="row">
                                    <!-- Widget Column -->
                                    <div class="col-md-6">
                                        <!-- Widget -->
                                        <div class="widget subscribe no-box">
                                            <h5 class="widget-title">Newsletter<span></span></h5>
                                            <p class="form-message1" style="display: none;"></p>
                                            <div class="clearfix"></div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nec odio ipsum. Suspendisse cursus malesuada facilisis.Lorem ipsum dolor sit amet, consectetur.</p>
                                            <form class="input-group subscribe-form" name="subscribe-form" method="post" action="http://glorytheme.com/template/universh/demo/php/subscription.php" id="subscribe-form">
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="email" placeholder="Subscribe" value="" name="subscribe_email" id="subscribe_email">
                                                </div>
                                                <span class="input-group-btn">
                                                    <a class="btn"><span class="glyphicon glyphicon-arrow-right"></span></a>
                                                </span>
                                            </form>
                                        </div><!-- Widget -->
                                    </div><!-- Column -->


                                    <!-- Widget Column -->
                                    <div class="col-md-3">
                                        <!-- Widget -->
                                        <div class="widget no-box">
                                            <h5 class="widget-title">UseFull Links<span></span></h5>
                                            <li><a href="admin/Web_owner_info/show_web_owner_profile">Qec</a></li>
                                            <li><a href="admin/Programe/show_user_side_programe/3">Pb</a></li>
                                            <li><a href="admin/Alumini/show_user_side_alumin">Alumini</a></li>
                                            <li><a href="admin/Events/show_user_side_events">Events</a></li>
                                            <li><a href="admin/News/show_user_side_news">News</a></li>
                                        </div><!-- Widget -->
                                        <!-- Widget -->
                                        <div class="widget no-box">
                                            <h5 class="widget-title">Follow Us<span></span></h5>
                                            <!-- Social Icons Color -->
                                            <ul class="social-icons color">
                                                <li class="facebook"><a href="<?php echo $Web_owner_info_model[0]['email'] ?>" target="_blank" title="Facebook">Facebook</a></li>
                                                <li class="twitter"><a href="<?php echo $Web_owner_info_model[0]['twitter'] ?>" target="_blank" title="Twitter">Twitter</a></li>
                                                <li class="linkedin"><a href="<?php echo $Web_owner_info_model[0]['linkden'] ?>" target="_blank" title="Linkedin">Linkedin</a></li>
                                                <li class="googleplus"><a href="<?php echo $Web_owner_info_model[0]['g_plus'] ?>" target="_blank" title="googleplus">googleplus</a></li>
                                            </ul>   
                                        </div><!-- Widget -->
                                    </div><!-- Column -->

                                    <!-- Widget Column -->
                                    <div class="col-md-3">
                                        <!-- Widget -->
                                        <div class="widget gallery-widget no-box">
                                            <h5 class="widget-title">College Gallery<span></span></h5>
                                            <!-- Gallery Widget -->
                                            <ul>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-01.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-02.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-03.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-04.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-05.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-06.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-07.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-08.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-09.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-10.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-11.jpg" class="img-responsive" alt="Thumb"></a></li>
                                                <li><a href="#"><img width="60" height="60" src="assets1/images/default/gallery-thumb-12.jpg" class="img-responsive" alt="Thumb"></a></li>
                                            </ul>
                                            <p><a href="www.glorythemes.html" title="glorythemes">@Glorythemes</a></p>
                                        </div><!-- Widget -->
                                    </div><!-- Column -->

                                </div><!-- Row -->
                            </div><!-- Container -->        
                        </div><!-- Main Footer -->

                        <!-- Footer Copyright -->
                        <div class="footer-copyright">
                            <div class="container">
                                <div class="row">
                                    <!-- Copy Right Logo -->
                                    <div class="col-md-2">
                                        <a class="logo" href="index.html">
                                            <img src="images/<?php echo $Web_owner_info_model[0]['logo'] ?>" width="211" height="40"  class="img-responsive" alt="Universh Education HTML5 Website Template">
                                        </a>
                                    </div><!-- Copy Right Logo -->
                                    <!-- Copy Right Content -->
                                    <div class="col-md-6">
                                        <p>&copy; Copyright 2018. All Rights Reserved. | By <?php echo $Web_owner_info_model[0]['name'] ?></p>
                                    </div><!-- Copy Right Content -->
                                    <!-- Copy Right Content -->
                                    <div class="col-md-4">

                                    </div><!-- Copy Right Content -->
                                </div><!-- Footer Copyright -->
                            </div><!-- Footer Copyright container -->
                        </div><!-- Footer Copyright -->
                    </footer>
                    <!-- Footer -->

                    <!-- library -->
                    <script type="text/javascript">
                        $('.btn').click(function()
                        {
                            var value=$('#subscribe_email').val();
                            $.ajax({
                                url:'<?php echo base_url('admin/Subscriber/insertion')?>',
                                method:'POST',
                                data:{value:value},
                                success:function(data)
                                {
                                  $('#subscribe_email').val('');
                              }
                          });
                        });
                    </script>



                    <script src="assets1/js/lib/bootstrap.min.js"></script>
                    <script src="assets1/js/lib/bootstrapValidator.min.js"></script>
                    <script src="assets1/js/lib/jquery.appear.js"></script>
                    <script src="assets1/js/lib/jquery.easing.min.js"></script>
                    <script src="assets1/js/lib/owl.carousel.min.js"></script>
                    <script src="assets1/js/lib/countdown.js"></script>
                    <script src="assets1/js/lib/counter.js"></script>
                    <script src="assets1/js/lib/isotope.pkgd.min.js"></script>
                    <script src="assets1/js/lib/jquery.easypiechart.min.js"></script>
                    <script src="assets1/js/lib/jquery.mb.YTPlayer.min.js"></script>
                    <script src="assets1/js/lib/jquery.prettyPhoto.js"></script>
                    <script src="assets1/js/lib/jquery.stellar.min.js"></script>
                    <script src="assets1/js/lib/menu.js"></script>



                    <!-- Revolution Js -->
                    <script src="assets1/revolution/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
                    <script src="assets1/revolution/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>
                    <script src="assets1/js/lib/theme-rs.js"></script>

                    <script src="assets1/js/lib/modernizr.js"></script>
                    <script src="assets1/js/lib/modernizr.js"></script>
                    <!-- Theme Base, Components and Settings -->
                    <script src="assets1/js/theme.js"></script>

                    <!-- Theme Custom -->
                    <script src="assets1/js/custom.js"></script>

                </body>

                <!-- Mirrored from glorytheme.com/template/universh/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 08:22:33 GMT -->
                </html>