<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Manager/insertion" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Enter Name</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control form-control-square" id="input-17"  placeholder="Enter Name">
             </div>
           </div>
         <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="text" name="email" class="form-control form-control-square" id="input-17"  placeholder="Email">
             </div>

           </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="text" name="password" class="form-control form-control-square" id="input-17"  placeholder="Password">
             </div>
        </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Phone Number</label>
              <div class="col-sm-10">
                <input type="text" name="phone" class="form-control form-control-square" id="input-17"  placeholder="Phone Number">
             </div>

           </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" class="form-control form-control-square" id="input-17"  placeholder="Address">
             </div>
           </div>
    
           <div class="form-group row">
                  <label for="input-8" class="col-sm-2 col-form-label">Select Image</label>
                  <div class="col-sm-10">
                    <input type="file" class="form-control" id="input-8" name="photo" required>
                  </div>
             </div>

  

     <div class="form-group">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

  </form>

</div>
</div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
<script type="text/javascript">

  $(document).on('change','#category_name',function() 
  {
      var temp = '';
      var value=$('#category_name').val();
      $.ajax
      ({
        url:'<?php echo base_url('admin/Subcategory/ajax_value')?>',
        method:'POST',
        data:{value:value},
        dataType:"json",
        success:function(data)
        {
          data.forEach((val,ind)=>{
            temp += `<option>${val.name}</option>`;
          });
          $(".appenddata").html(temp);
          console.log(temp);
         
        }
      });
  })
</script>