
<!-- Page Header -->
<div class="page-header bg-dark">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- Page Header Wrapper -->
				<div class="page-header-wrapper">
					<!-- Title & Sub Title -->
					<h3 class="title">Qec</h3>
					<h6 class="sub-title">All you want know</h6>
					<ol class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li class="active">Qec</li>
					</ol><!-- Breadcrumb -->
				</div><!-- Page Header Wrapper -->
			</div><!-- Coloumn -->
		</div><!-- Row -->
	</div><!-- Container -->
</div><!-- Page Header -->

<!-- Page Main -->
<div role="main" class="main">
	<div class="page-default bg-grey typo-dark">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<!-- Sidebar -->
				<div class="col-md-8">
					<!-- aside -->
					<aside class="sidebar">
						<!-- Widget -->
						<div class="widget no-box">
							
						</div><!-- Widget -->
						
						<!-- Widget -->
						<div class="widget">
							<h4>Description</h4>
							<p><?php echo $key[0]['details']?></p>


						</div>
						

						

					</aside><!-- aside -->	
				</div><!-- Column -->
				
				<!-- Page Content -->
				<div class="col-md-4">
					<div class="row">
						<!-- Event Column -->
						<div class="col-sm-12 event-single-wrap">
							<!-- Event Wrapper -->
							<div class="row">
								<!-- Event Image Wrapper -->
								<br><br>
								<div class="col-sm-12">
									<div class="event-img-wrap">
										<img alt="Event" class="img-responsive" src="images/<?php echo $key[0]['logo']?>" width="800" height="700">
									</div>
								</div><!-- Event Image Wrapper -->
								<!-- Event Detail Wrapper -->
								<div class="col-sm-12">
									<div class="event-single-details margin-top-30">
										<h3><?php echo $key[0]['name']?></h3>
										<!-- Event Count -->
										<div class="row count-container">
											<div class="col-sm-6">

											</div>
											
										</div><!-- Row -->

									</div><!-- Event Single Detail -->
								</div><!-- Column -->

							</div><!-- Event Wrapper -->
							
							<!-- Divider -->
							<hr class="md">

						</div><!-- Column -->
						
					</div><!-- Row -->


				</div><!-- Column -->
			</div><!-- Row -->
		
			<section class="element-animation-section bg-grey">
				<div class="container">
					<?php
					$i=1; 
					foreach ($single_department as $value) 
					{ 

						$heading = explode(',', $value['heading']);
						$paragraph = explode(',', $value['paragraph']);

						$fullPara = array_combine($heading, $paragraph);
						// print_r($fullPara);
						// if($fullPara != '')
						// {
							foreach ($fullPara as $key => $value)
							{
								if($key != '' && $value != ''){
								?>
								<div class="row">
									<div class="col-md-10">
										<!-- Tab -->
										<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="headingTwo">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1<?php echo $i?>" aria-expanded="false" aria-controls="collapseTwo">
															<?php echo $key ?>
														</a>
													</h4>
												</div>
												<div id="collapseTwo1<?php echo $i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="panel-body">
														<p><?php echo $value?>.</p>
													</div>
												</div>
											</div>

										</div><!-- Tab -->
									</div><!-- Column -->

								</div><!-- Row -->
								<?php  
								$i++;  
								}
							}
							
					
					
				}
					?>


				</div><!-- Container -->
			</section><!-- Section -->
		
		</div><!-- Container -->

	</div><!-- Page Default -->
</div><!-- Page Main -->

