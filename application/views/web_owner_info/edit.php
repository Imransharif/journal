<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Web_owner_info/update" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <input type="hidden" name="id" value="<?php echo $data[0]['id'] ?>">
              <input type="hidden" name="tmp_image" value="<?php echo $data[0]['logo'] ?>">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Enter Name</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control form-control-square" id="input-17"  placeholder="Enter Name" value="<?php echo $data[0]['name']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control form-control-square" id="input-17"  placeholder="Email" value="<?php echo $data[0]['email']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="text" name="password" class="form-control form-control-square" id="input-17"  placeholder="Password" value="<?php echo $data[0]['password']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Phone Number</label>
              <div class="col-sm-10">
                <input type="text" name="phone" class="form-control form-control-square" id="input-17"  placeholder="Phone Number" value="<?php echo $data[0]['phone']?>">
              </div>

            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Facebook_link</label>
              <div class="col-sm-10">
                <input type="text" name="facebook" class="form-control form-control-square" id="input-17"  placeholder="Facebook_link" value="<?php echo $data[0]['facebook']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Instagram_link</label>
              <div class="col-sm-10">
                <input type="text" name="instagram" class="form-control form-control-square" id="input-17"  placeholder="Instagram_link" value="<?php echo $data[0]['instagram']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Twitter_link</label>
              <div class="col-sm-10">
                <input type="text" name="twitter" class="form-control form-control-square" id="input-17"  placeholder="Twitter_link" value="<?php echo $data[0]['twitter']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">G_Plus</label>
              <div class="col-sm-10">
                <input type="text" name="g_plus" class="form-control form-control-square" id="input-17"  placeholder="G_Plus" value="<?php echo $data[0]['g_plus']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Viemo_link</label>
              <div class="col-sm-10">
                <input type="text" name="viemo" class="form-control form-control-square" id="input-17"  placeholder="Viemo_link" value="<?php echo $data[0]['viemo']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Linkdin_link</label>
              <div class="col-sm-10">
                <input type="text" name="linkden" class="form-control form-control-square" id="input-17"  placeholder="Linkdin_link" value="<?php echo $data[0]['linkden']?>">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" class="form-control form-control-square" id="input-17"  placeholder="Address" value="<?php echo $data[0]['adress']?>">
              </div>
            </div>
            <div class="department_desc">
              <div class="form-group row ">
               <label for="input-17" class="col-sm-2 col-form-label ">Description</label>
               <div class="col-sm-10">
                <textarear class="summernote" value="<?php echo $data[0]['details']?>"><?php echo $data[0]['details']?></textarear>
              </div>
            </div>
          </div>
      
          <div class="form-group row">
            <label for="input-8" class="col-sm-2 col-form-label">Select Logo</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="input-8" name="photo" >
              <img src="images/<?php echo $data[0]['logo'] ?>" width="150px" height="150px">
            </div>
          </div>



          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary">
          </div>

        </form>

      </div>
    </div>
  </div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
<script type="text/javascript">

  $(document).on('change','#category_name',function() 
  {
    var temp = '';
    var value=$('#category_name').val();
    $.ajax
    ({
      url:'<?php echo base_url('admin/Subcategory/ajax_value')?>',
      method:'POST',
      data:{value:value},
      dataType:"json",
      success:function(data)
      {
        data.forEach((val,ind)=>{
          temp += `<option>${val.name}</option>`;
        });
        $(".appenddata").html(temp);
        console.log(temp);

      }
    });
  })
</script>