

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Data Tables</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li>
          <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Image</th>
                    <th>View</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tr>
                  <?php 
                  ?>
                  <td><?php echo $key[0]['name'] ?></td>
                  <td><?php echo $key[0]['phone'] ?></td>
                  <td><?php echo $key[0]['email'] ?></td>
                  <td><?php echo $key[0]['adress'] ?></td>
                  <td><img src="<?php echo base_url()?>images/<?php echo $key[0]['logo'] ?> " style="height: 100px;width: 100px"></td>

                  <td><a href="admin/Web_owner_info/view/<?php echo $key[0]['id'] ?>" class="btn btn-sm btn-primary">View</a></td>
                  <td><a href="admin/Web_owner_info/edit/<?php echo $key[0]['id'] ?>" class="btn btn-sm btn-success">Edit</a></td>

                    <?php

                    ?>
                  </tr>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->


</div><!--End wrapper-->


<script>
 $(document).ready(function() {
      //Default data table
      $('#default-datatable').DataTable();


      var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );

      table.buttons().container()
      .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
    } );

  </script>

</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:04:05 GMT -->
</html>
