<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Web_owner_info/update" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <input type="hidden" name="id" value="<?php echo $data[0]['id'] ?>">
              <input type="hidden" name="tmp_image" value="<?php echo $data[0]['logo'] ?>">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Enter Name</label>
              <div class="col-sm-10">
                <input type="text" name="name" class="form-control form-control-square" id="input-17"  placeholder="Enter Name" value="<?php echo $data[0]['name']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control form-control-square" id="input-17"  placeholder="Email" value="<?php echo $data[0]['email']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="text" name="password" class="form-control form-control-square" id="input-17"  placeholder="Password" value="<?php echo $data[0]['password']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Phone Number</label>
              <div class="col-sm-10">
                <input type="text" name="phone" class="form-control form-control-square" id="input-17"  placeholder="Phone Number" value="<?php echo $data[0]['phone']?>" readonly>
              </div>

            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Facebook_link</label>
              <div class="col-sm-10">
                <input type="text" name="facebook" class="form-control form-control-square" id="input-17"  placeholder="Facebook_link" value="<?php echo $data[0]['facebook']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Instagram_link</label>
              <div class="col-sm-10">
                <input type="text" name="instagram" class="form-control form-control-square" id="input-17"  placeholder="Instagram_link" value="<?php echo $data[0]['instagram']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Twitter_link</label>
              <div class="col-sm-10">
                <input type="text" name="twitter" class="form-control form-control-square" id="input-17"  placeholder="Twitter_link" value="<?php echo $data[0]['twitter']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">G_Plus</label>
              <div class="col-sm-10">
                <input type="text" name="g_plus" class="form-control form-control-square" id="input-17"  placeholder="G_Plus" value="<?php echo $data[0]['g_plus']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Viemo_link</label>
              <div class="col-sm-10">
                <input type="text" name="viemo" class="form-control form-control-square" id="input-17"  placeholder="Viemo_link" value="<?php echo $data[0]['viemo']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Linkdin_link</label>
              <div class="col-sm-10">
                <input type="text" name="linkden" class="form-control form-control-square" id="input-17"  placeholder="Linkdin_link" value="<?php echo $data[0]['linkden']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Address</label>
              <div class="col-sm-10">
                <input type="text" name="address" class="form-control form-control-square" id="input-17"  placeholder="Address" value="<?php echo $data[0]['adress']?>" readonly>
              </div>
            </div>
            <div class="department_desc">
              <div class="form-group row ">
               <label for="input-17" class="col-sm-2 col-form-label ">Description</label>
               <div class="col-sm-10">
                <textarear class="summernote" value="<?php echo $data[0]['details']?>"><?php echo $data[0]['details']?></textarear>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="input-8" class="col-sm-2 col-form-label"> Logo</label>
            <div class="col-sm-10">
             
              <img src="images/<?php echo $data[0]['logo'] ?>" width="150px" height="150px">
            </div>
          </div>



          <div class="form-group">
            <a href="admin/Web_owner_info/show_web_owner_info" class="btn btn-sm btn-primary">Go to Back</a>
          </div>

        </form>

      </div>
    </div>
  </div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
