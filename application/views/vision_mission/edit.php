<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Vision/update" method="POST" enctype="multipart/form-data">
           <div class="department_desc">
            <div class="form-group row ">
              <input type="hidden" name="id" value="<?php echo $data[0]['id'] ?>">
             <label for="input-17" class="col-sm-2 col-form-label ">Vision Mission</label>
             <div class="col-sm-10">
              <textarear class="summernote" value="<?php echo $data[0]['vision'] ?>"><?php echo $data[0]['vision'] ?></textarear>
            </div>
          </div>
        </div>

        <div id="n_cl">

        </div>


        <div class="form-group">
          <input type="submit" name="submit" class="btn btn-primary">
        </div>

      </form>

    </div>
  </div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
