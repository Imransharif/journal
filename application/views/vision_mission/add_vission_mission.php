<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Vision/insertion" method="POST" enctype="multipart/form-data">   
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Vision Mission</label>
              <div class="col-sm-10">
                <textarea class="form-control" rows="7" name="vision"></textarea>
             </div>
           </div>
         
  
     <div class="form-group">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

  </form>

</div>
</div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
