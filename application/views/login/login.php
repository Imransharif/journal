<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:01:20 GMT -->
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Quaid Azam-e-University</title>

	<!--favicon-->
	<base href="<?php echo base_url()?>">
	<script>
		var base_url = '<?php echo base_url();?>';
	</script>
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
	<!-- Bootstrap core CSS-->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- animate CSS-->
	<link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
	<!-- Icons CSS-->
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
	<!-- Custom Style-->
	<link href="assets/css/app-style.css" rel="stylesheet"/>

</head>

<body class="authentication-bg">
	<!-- Start wrapper-->
	<div id="wrapper">
		<div class="card card-authentication1 mx-auto my-5 animated zoomIn">
			<div class="card-body">
				<div class="card-content p-2">
					<?php
					if($this->session->flashdata('error_message'))
					{
						?>
						<div style="font-size: 16px;color: red">
							<strong>Alert:</strong><?php echo $this->session->flashdata('error_message')?>
						</div>
						<?php 
					}
					?>
					<div class="text-center">
						<img src="assets/images/logo-icon.png"/>
					</div>
					<div class="card-title text-uppercase text-center py-2">Sign In</div>
					<form action="<?php echo base_url('admin/Login/login')?>" method="POST">
						<div class="form-group">
							<div class="position-relative has-icon-left">
								<label for="exampleInputUsername" class="sr-only">Username</label>
								<input type="text" id="exampleInputUsername" class="form-control" placeholder="Username" name="email">
								<div class="form-control-position">
									<i class="icon-user"></i>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="position-relative has-icon-left">
								<label for="exampleInputPassword" class="sr-only">Password</label>
								<input type="password" id="exampleInputPassword" class="form-control" placeholder="Password" name="password">
								<div class="form-control-position">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="position-relative has-icon-left">
								<label for="exampleInputPassword" class="sr-only">Password</label>
								<select class="form-control" name="status">
									<option value="Director_Qec">Director_Qec</option>
									<option value="Director_Pb">Director_Pb</option>
									<option value="Manager">Manager</option>
									<option value="Cordinator">Cordinator</option>
									<option value="Teacher">Teacher</option>						
									<option value="Student">Student</option>
								</select>
								<div class="form-control-position">
									<i class="icon-user"></i>
								</div>
							</div>
						</div>
			<!-- <div class="form-row mr-0 ml-0">
			 <div class="form-group col-6">
			   <div class="icheck-material-primary">
                <input type="checkbox" id="user-checkbox" checked="" />
                <label for="user-checkbox">Remember me</label>
			  </div>
			 </div>
			 <div class="form-group col-6 text-right">
			  <a href="<?php echo base_url('admin/Login/forgetpassword_load')?>">Reset Password</a>
			 </div>
			</div> -->
			<div class="form-group">
				<button type="submit" class="btn btn-danger shadow-danger btn-block waves-effect waves-light">Sign In</button>
			</div>
			  <!-- <div class="form-group text-center">
			   <p class="text-muted">Not a Member ? <a href="authentication-signup.html"> Sign Up here</a></p>
			</div> -->
			<!--  <div class="form-group text-center">
			    <hr>
				<h5>OR</h5>
			</div> -->
			  <!-- <div class="form-group text-center">
				<button type="button" class="btn btn-facebook shadow-facebook text-white btn-block waves-effect waves-light"><i class="fa fa-facebook-square"></i> Sign In With Facebook</button>
			</div> -->
		</form>
	</div>
</div>
</div>

<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
</div><!--wrapper-->

<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>

</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:01:20 GMT -->
</html>
