<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-reset-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:01:21 GMT -->
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <base href="<?php echo base_url()?>">
  <script type="text/javascript">
  	var base_url='<?php echo base_url()?>';
  </script>
  <title>Quaid Azam-e-University</title>
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap core CSS-->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Custom Style-->
  <link href="assets/css/app-style.css" rel="stylesheet"/>
  
</head>

<body class="authentication-bg">
 <!-- Start wrapper-->
 <div id="wrapper">
	<div class="card card-authentication1 mx-auto my-5 animated rollIn">
		<div class="card-body">
		 <div class="card-content p-2">
		  <div class="card-title text-uppercase text-center pb-2">Reset Password</div>
		    <p class="text-center pb-2">Please enter your email address. You will receive a link to create a new password via email.</p>
		    <form  action="<?php echo base_url('admin/Login/sendmail')?>" method="POST">
			  <div class="form-group">
			   <div class="position-relative has-icon-left">
				  <label for="exampleInputEmailAddress" class="sr-only">Email Address</label>
				  <input type="text" id="exampleInputEmailAddress" class="form-control" placeholder="Email Address " name="email">
				  <div class="form-control-position">
					  <i class="icon-envelope-open"></i>
				  </div>
			   </div>
			  </div>
			 
			  <button type="submit" class="btn btn-danger shadow-danger btn-block waves-effect waves-light mt-3">Reset Password</button>
			  <div class="text-center pt-3">
				<hr>
				<p class="text-muted">Return to the <a href="authentication-signin.html"> Sign In</a></p>
			  </div>
			 </form>
		   </div>
		  </div>
	     </div>
    
     <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	</div><!--wrapper-->
	
  <!-- Bootstrap core JavaScript-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- waves effect js -->
  <script src="assets/js/waves.js"></script>
  <!-- Custom scripts -->
  <script src="assets/js/app-script.js"></script>
	
</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-reset-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:01:21 GMT -->
</html>
