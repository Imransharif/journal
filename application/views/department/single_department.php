

<!-- Page Header -->
<div class="page-header bg-dark">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- Page Header Wrapper -->
				<div class="page-header-wrapper">
					<!-- Title & Sub Title -->
					<h3 class="title"> 


						<?php
						$programe;
						if($single_department[0]['programe_id']==1)
						{
							echo  $programe= 'Graduate';
						}
						if($single_department[0]['programe_id']==2)
						{
							echo  $programe= 'UnderGraduate';
						}
						if($single_department[0]['programe_id']==3)
						{
							echo  $programe= 'PostGraduate';
						}

						?>



					</h3>
					<h6 class="sub-title">All you want know</h6>
					<ol class="breadcrumb">
						<li><a>Home</a></li>
						<li class="active"> <?php echo  $programe;?></li>
						<li class="active"> <?php echo $single_department[0]['category_name'];?></li>
					</ol><!-- Breadcrumb -->
				</div><!-- Page Header Wrapper -->
			</div><!-- Coloumn -->
		</div><!-- Row -->
	</div><!-- Container -->
</div><!-- Page Header -->

<!-- Page Main -->
<div role="main" class="main">
	<div class="page-default bg-grey typo-dark">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<!-- Page Content -->
				<div class="col-md-9">
					<div class="row">
						<!-- Blog Column -->
						<div class="col-xs-12 blog-single">
							<div class="blog-single-wrap">
								<div class="blog-img-wrap">
									<img width="1920" height="700" src="images/<?php echo $single_department[0]['image']?>" class="img-responsive" alt="Event">
								</div>
								<!-- Blog Detail Wrapper -->
								<div class="blog-single-details">
									<h4><a href="#"><?php echo $single_department[0]['department_name'];?></a></h4>
									<!-- Blog Description -->
									<p><?php echo $single_department[0]['department_desc']?></p>

								</div><!-- Blog Detail Wrapper -->
							</div><!-- Blog Wrapper -->
							
							<!-- Blog Share Post -->
							<!-- Section -->
							<section class="element-animation-section bg-grey">
								<div class="container">
									<?php foreach ($single_department as $value) 
									{ 
										
										$heading = explode(',', $value['heading']);
										$paragraph = explode(',', $value['paragraph']);
										$fullPara = array_combine($heading, $paragraph);
										$i=1;
										foreach ($fullPara as $key => $value)
										{
											if($key != '' && $value != ''){
												?>
												

												<div class="row">
													<div class="col-md-12">
														<!-- Tab -->
														<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
															<div class="panel panel-default">
																<div class="panel-heading" role="tab" id="headingTwo">
																	<h4 class="panel-title">
																		<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1<?php echo $i?>" aria-expanded="false" aria-controls="collapseTwo">
																			<?php echo $key ?>
																		</a>
																	</h4>
																</div>
																<div id="collapseTwo1<?php echo $i?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
																	<div class="panel-body">
																		<p><?php echo $value?>.</p>
																	</div>
																</div>
															</div>

														</div><!-- Tab -->
													</div><!-- Column -->

												</div><!-- Row -->
												<?php  
												$i++;  

											}
										}
									}
									?>


								</div><!-- Container -->
							</section><!-- Section -->
							<div class="share">
							
							</div><!-- Blog Share Post -->

							<!-- Related Post -->
							<?php 
							if(!empty($remaining_department))
							{
								?>
								<h4>More Department : </h4>
								<div class="owl-carousel" 
								data-animatein="" 
								data-animateout="" 
								data-margin="30" 
								data-loop="true" 
								data-merge="true" 
								data-nav="true" 
								data-dots="false" 
								data-stagepadding="" 
								data-items="1" 
								data-mobile="1" 
								data-tablet="3" 
								data-desktopsmall="3"  
								data-desktop="3" 
								data-autoplay="true" 
								data-delay="3000" 
								data-navigation="true">
								<?php  
								foreach ($remaining_department as  $value) 
								{
									?>

									<div class="item">
										<!-- Related Wrapper -->
										<div class="related-wrap">
											<!-- Related Image Wrapper -->
											<div class="img-wrap">
												<img width="600" height="220" alt="Blog" class="img-responsive" src="images/<?php echo $value['image']?>">
											</div>
											<!-- Related Content Wrapper -->
											<div class="related-content">
												<a href="admin/Department/single_department/<?php echo $value['department_id']?>" title="Read More">+</a><span><?php echo substr($value['department_desc'],0,20)?></span>
												s
											</div><!-- Related Content Wrapper -->
										</div><!-- Related Wrapper -->
									</div><!-- Item -->
									<?php  
								}
							}
							?>
						</div><!-- Related Post -->

					</div><!-- Column -->
				</div><!-- Row -->
			</div><!-- Column -->
			<!-- Sidebar -->
			<div class="col-md-3">
				<!-- aside -->
				<aside class="sidebar">

					<!-- Widget -->
					<?php
					 $id=$single_department[0]['department_id'];  
					$related_depertment=related_depertment($value['category_id']);
					if(!empty($related_depertment))
						{
							?>
							<div class="widget">
								<h5 class="widget-title">Related Department<span></span></h5>
								<ul class="thumbnail-widget">

									<?php 

									foreach ($related_depertment as  $value) 
									{
										if($id!=$value['department_id'])
										{
										?>

										<li>
											<div class="thumb-wrap">
												<img width="60" height="60" alt="Thumb" class="img-responsive" src="images/<?php echo $value['image']?>">
											</div>
											<div class="thumb-content"><a href="#"><?php echo substr($value['department_desc'],0,20)?></div>	
											</li>

											<?php  
										}
									}
										?>
									</ul><!-- Thumbnail Widget -->
								</div><!-- Widget -->
							<?php } ?>


							
						</aside><!-- aside -->	
					</div><!-- Column -->
				</div><!-- Row -->
			</div><!-- Container -->
		</div><!-- Page Default -->
	</div><!-- Page Main -->

