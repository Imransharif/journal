<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Department/update" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <input type="hidden" name="id" value="<?php echo $department[0]['department_id']?>"> 
              <input type="hidden" name="tmp_img" value="<?php echo $department[0]['image']?>">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Programe</label>
              <div class="col-sm-10">
                <select class="form-group form-control" name="programe_name">
                  <?php if($department[0]['programe_id'] == 1)
                  {
                    ?>

                    <option value="1" selected>Graduate</option>
                    <option value="2">UnderGraduate</option>
                    <option value="3">PostGraduate</option>

                    
                    <?php
                  } 
                  if($department[0]['programe_id'] == 2)
                  {
                    ?>

                    <option value="1" >Graduate</option>
                    <option value="2" selected>UnderGraduate</option>
                    <option value="3">PostGraduate</option>

                    <?php 
                  }
                  if($department[0]['programe_id'] == 3)
                  {
                    ?>

                    <option value="1" >Graduate</option>
                    <option value="2">UnderGraduate</option>
                    <option value="3" selected>PostGraduate</option>
                    <?php 
                  } 
                  ?>

                </select>
              </div>

            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Category</label>
              <div class="col-sm-10">
                <select class="form-group form-control appenddata" name="category_name">
                 <?php foreach ($category as $key1)
                 { 
                   ?>
                   <option value="<?php echo $key1['id']?>"><?php echo $key1['category_name']?></option>
                   <?php 
                 } 
                 ?>
               </select>
             </div>
           </div>
           <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label">Enter Department  Name</label>
             <div class="col-sm-10">
              <input type="text" name="department_name" class="form-control form-control-square" id="input-17"  placeholder="Enter Program List Point Name" value="<?php echo $department[0]['department_name']?>">
            </div>
          </div>
          <div class="department_desc">
            <div class="form-group row ">
             <label for="input-17" class="col-sm-2 col-form-label ">Enter Department  Description</label>
             <div class="col-sm-10">
              <textarear class="summernote" value="<?php echo $department[0]['department_desc']?>"><?php echo $department[0]['department_desc']?></textarear>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="input-8" class="col-sm-2 col-form-label">Select Image</label>
          <div class="col-sm-10">
            <input type="file" class="form-control" id="input-8" name="photo">
            <img src="images/<?php echo $department[0]['image']?>" width="150" height="150">
          </div>
        </div>
        <?php foreach ($department as  $value) 
        {
         $heading = explode(',', $value['heading']);
         $paragraph = explode(',', $value['paragraph']);
         $fullPara = array_combine($heading, $paragraph);
         foreach ($fullPara as $key => $value)
         {
           ?>

           <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label">Enter Program List Point Name</label>
             <div class="col-sm-10">
              <input type="text" name="p_point[]" class="form-control form-control-square" id="input-17"  placeholder="Enter Program List Point Name" value="<?php echo $key ?>">
            </div>
          </div>
          <div class="note-editable">
            <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label ">Enter Program Point Description</label>
             <div class="col-sm-10">
              <div class="summernote" value="<?php echo $value?>"><?php echo $value?></div>
            </div>
          </div>
        </div>
        <?php 
        } 
      } 
      ?>
      <div id="n_cl">

      </div>
     <!--  <div class="col-md-12">
       <div class="col-md-10 pull-right">
         <center><button type="button" class="btn btn-primary shadow-primary waves-effect waves-light m-1 add_clone">Add One More Point In List</button></center>
       </div>
     </div> -->

     <div class="form-group">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

  </form>

</div>
</div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
<script type="text/javascript">

  $(document).on('change','#programe_name',function() 
  {
    var temp = '';
    var value=$('#programe_name').val();
    $.ajax
    ({
      url:'<?php echo base_url('admin/Category/ajax_value')?>',
      method:'POST',
      data:{value:value},
      dataType:"json",
      success:function(data)
      {
        data.forEach((val,ind)=>{
          temp += `<option value="${val.id}">${val.category_name}</option>`;
        });
        $(".appenddata").html(temp);
        console.log(temp);

      }
    });
  })
</script>