

<div class="clearfix"></div>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Data Tables</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li>
          <li class="breadcrumb-item active" aria-current="page">Data Tables</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div>
          <div class="card-body">
            <?php 

            if($this->session->userdata('status')=='Director_Qec')
            {
             ?>
             <a href="admin/Department/">
              <button class="btn btn-primary">Add New</button><br>
              <?php 
            }
            ?>
          </a>
          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Department_Name</th>
                  <th>Category_Name</th>
                  <th>Programe_Name</th>
                  <th>Image</th>
                  <th>Action</th>
                  <th>View</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>

                <?php 
                foreach ( $department as $value) 
                {

                  ?>
                  <tr>
                    <td><?php echo $value['department_id'] ?></td>
                    <td><?php echo $value['department_name'] ?></td>
                    <td><?php echo $value['category_name'] ?></td>
                    <td><?php 
                    if($value['programe_id']==1)
                    {
                      echo 'Graduate';
                    }
                    if($value['programe_id']==2)
                    {
                      echo 'Undergraduate';
                    }
                    if($value['programe_id']==3)
                    {
                      echo 'Postgraduate';
                    } 
                    ?>
                  </td>
                  <td><img src="<?php echo base_url()?>images/<?php echo $value['image'] ?> " style="height: 100px;width: 100px"></td>
                  <?php 
                  if($this->session->userdata('status')=='Director_Qec')
                  {
                   ?>

                   <td >
                    <?php 
                    $id = $value['department_id'];
                    $status = $value['dep_status'];

                    if ($status == 0) {

                      ?>

                      <a href="javascript:;" class="btn btn-sm btn-danger" 
                      onclick="featured(this,<?php echo $id.",".$status; ?>);">Unfeatured</a>

                      <?php 
                    } else {
                      ?>

                      <a href="javascript:;" class="btn btn-sm btn-success" 
                      onclick="featured(this,<?php echo $id.",".$status; ?>);">Featured</a>
                      <?php  
                    } 
                    ?>
                  </td>
              <td><a href="admin/Department/view/<?php echo $value['department_id'] ?>" class="btn btn-sm btn-primary">View</a></td>
                    <td><a href="admin/Department/edit/<?php echo $value['department_id'] ?>" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-2x" ></i></a></td>
                      <td><a href="admin/Department/delete/<?php echo $value['department_id'] ?>" class="btn btn-sm btn-danger"><i class='fa fa-trash fa-2x'></i></a></td>
                        <?php 
                      } 
                      ?>
                    </tr>
                    <?php 
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->


</div><!--End wrapper-->


<script>
 $(document).ready(function() {
      //Default data table
      $('#default-datatable').DataTable();


      var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );

      table.buttons().container()
      .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
    } );

  </script>
  <script type="text/javascript">
    function featured($this,$id,$status)
    {
      var obj = $this;
      console.log(obj);
      var id=$id;
      var status=$status;
      $.ajax({
        url:'<?php echo base_url('admin/Department/update_status') ?>',
        method:'POST',
        data:{value:id,status:status},
        dataType: "json",
        success:function(data)
        {
          if(data.dep_status == 1){
           $(obj).replaceWith('<a href="javascript:;" onclick="featured(this,'+data.id+','+data.dep_status+')" class="btn btn-sm btn-success"> featured </a>');


         } else {
          $(obj).replaceWith('<a href="javascript:;" onclick="featured(this,'+data.id+','+data.dep_status+')" class="btn btn-sm btn-danger"> Unfeatured </a>');
        }
      }

    });

    }
  </script>
</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/table-data-tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Nov 2018 09:04:05 GMT -->
</html>
