<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Department/update" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <input type="hidden" name="id" value="<?php echo $department[0]['department_id']?>"> 
              <input type="hidden" name="tmp_img" value="<?php echo $department[0]['image']?>">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Programe</label>
              <div class="col-sm-10">
                  <?php if($department[0]['programe_id'] == 1)
                  {
                    ?>
                    <input type="text" name="department_name" class="form-control form-control-square" value="Graduate" readonly="">
                        
                    <?php
                  } 
                  if($department[0]['programe_id'] == 2)
                  {
                    ?>
                   <input type="text" name="department_name" class="form-control form-control-square" value="UnderGraduate" readonly="">

                    <?php 
                  }
                  if($department[0]['programe_id'] == 3)
                  {
                    ?>
                     <input type="text" name="department_name" class="form-control form-control-square" value="PostGraduate" readonly="">
                    <?php 
                  } 
                  ?>

              </div>

            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Category</label>
              <div class="col-sm-10">
             
                   <input type="text"class="form-control form-control-square" value="<?php echo $department[0]['category_name']?>" readonly="">
               </select>
             </div>
           </div>
           <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label"> Department  Name</label>
             <div class="col-sm-10">
              <input type="text" name="department_name" class="form-control form-control-square" id="input-17"   value="<?php echo $department[0]['department_name']?>" readonly="">
            </div>
          </div>
          <div class="department_desc">
            <div class="form-group row ">
             <label for="input-17" class="col-sm-2 col-form-label "> Department  Description</label>
             <div class="col-sm-10">
              <textarear class="summernote" readonly="" value="<?php echo $department[0]['department_desc']?>" ><?php echo $department[0]['department_desc']?></textarear>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="input-8" class="col-sm-2 col-form-label"> Image</label>
          <div class="col-sm-10">
          </div>
        </div>
        <div class="form-group row">
           <div class="col-sm-2"></div>
          <div class="col-sm-10">
              <img src="images/<?php echo $department[0]['image']?>" width="150" height="150">
          </div>
        </div>

        <?php foreach ($department as  $value) 
        {
         $heading = explode(',', $value['heading']);
         $paragraph = explode(',', $value['paragraph']);
         $fullPara = array_combine($heading, $paragraph);
         foreach ($fullPara as $key => $value)
         {
           ?>

           <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label"> Program List Point Name</label>
             <div class="col-sm-10">
              <input type="text" name="p_point[]" class="form-control form-control-square" id="input-17" readonly=""  value="<?php echo $key ?>">
            </div>
          </div>
          <div class="note-editable">
            <div class="form-group row">
             <label for="input-17" class="col-sm-2 col-form-label "> Program Point Description</label>
             <div class="col-sm-10">
              <div class="summernote" value="<?php echo $value?>" ><?php echo $value?></div>
            </div>
          </div>
        </div>
        <?php 
        } 
      } 
      ?>
      <div id="n_cl">

      </div>
     <!--  <div class="col-md-12">
       <div class="col-md-10 pull-right">
         <center><button type="button" class="btn btn-primary shadow-primary waves-effect waves-light m-1 add_clone">Add One More Point In List</button></center>
       </div>
     </div> -->

     <div class="form-group">
      <a href="admin/Department/show_department" class="btn btn-sm btn-primary">Go to Back</a>
    </div>

  </form>

</div>
</div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
<script type="text/javascript">

  $(document).on('change','#programe_name',function() 
  {
    var temp = '';
    var value=$('#programe_name').val();
    $.ajax
    ({
      url:'<?php echo base_url('admin/Category/ajax_value')?>',
      method:'POST',
      data:{value:value},
      dataType:"json",
      success:function(data)
      {
        data.forEach((val,ind)=>{
          temp += `<option value="${val.id}">${val.category_name}</option>`;
        });
        $(".appenddata").html(temp);
        console.log(temp);

      }
    });
  })
</script>