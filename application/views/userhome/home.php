
<div role="main" class="main">

    <!-- Section -->
    <section class="pad-none hero">
        <div class="owl-carousel full-screen dots-inline" data-animatein="" 
        data-animateout="" 
        data-items="1" data-margin="" 
        data-loop="true"
        data-merge="true" 
        data-nav="true" 
        data-dots="false" 
        data-stagepadding="" 
        data-mobile="1" 
        data-tablet="1" 
        data-desktopsmall="1" 
        data-desktop="1" 
        data-autoplay="false" 
        data-delay="3000" 
        data-navigation="true">
        <?php foreach ( $key as $value) 
        {
         ?>
         <div class="item">
            <img src="images/<?php echo $value['image'] ?>" alt="" class="img-responsive" height="1000" width="2000">
            <div class="container slider-content vmiddle text-left">
                <div class="row">
                    <div class="col-md-offset-6 col-md-6">
                        <h3 class="text-uppercase animated" data-animate="fadeInUp" data-animation-delay="1200"><?php echo $value['heading'] ?></h3>
                        <p class="animated" data-animate="fadeInUp" data-animation-delay="1700"><?php echo $value['paragraph'] ?>.</p>

                    </div>  
                </div>  
            </div>
        </div>
        <?php  
    } 
    ?>



</div>
</section><!-- Section -->


<!-- Section -->
<section class="pad-bottom-50 slider-below-section">
    <div class="container">
        <div class="slider-below-wrap bg-color typo-light">
            <div class="row">
                <!-- Column -->          
                <!-- Column -->
                <div class="col-sm-12">
                    <!-- Content Box -->
                    <div class="content-box text-center">
                        <!-- Icon Wraper -->
                        <div class="icon-wrap">
                            <i class="uni-business-woman"></i>
                        </div><!-- Icon Wraper -->
                        <!-- Content Wraper -->
                        <div class="content-wrap">
                            <h5 class="heading">Vision Mission</h5>
                            <p><?php echo $vission[0]['vision']?></p>
                        </div><!-- Content Wraper -->
                    </div><!-- Content Box -->
                </div><!-- Column -->
            </div><!-- Slider Below Wrapper --> 
        </div><!-- Row -->
    </div><!-- Container -->
</section><!-- Section -->
<!-- Section -->
<section class="pad-top-none typo-dark">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Title -->
            <div class="col-sm-12">
                <div class="title-container sm">
                    <div class="title-wrap">
                        <h3 class="title">Our Featured Department</h3>
                        <span class="separator line-separator"></span>
                    </div>
                </div>
            </div>
            <!-- Title -->

            <!-- Column -->
            <?php foreach ($feature_department as  $value) 
            {
                ?>

                <div class="col-sm-4">
                    <!-- Course Wrapper -->
                    <div class="course-wrapper">
                        <!-- Course Banner Image -->
                        <div class="course-banner-wrap">
                            <img width="600" height="220" src="images/<?php echo $value['image'] ?>" class="img-responsive" alt="Course">
                        </div><!-- Course Banner Image -->
                        <!-- Course Detail -->
                        <div class="course-detail-wrap">
                            <!-- Course Content -->
                            <div class="course-content">
                                <h5><a href="course-single-left.html"><?php echo $value['department_name'] ?></a></h5>
                                <p><?php echo substr($value['department_desc'],0,20)?></p>
                                <a class="btn" href="#">Apply Now</a>
                            </div><!-- Course Content -->
                        </div><!-- Course Detail -->
                    </div><!-- Course Wrapper -->
                </div><!-- Column -->
                <?php  
            } 
            ?>

        </div><!-- Row -->
    </div><!-- Container -->
</section><!-- Section -->

<!-- Section -->
<section class="bg-dark typo-light">
    <div class="container">
        <div class="row counter-sm">
            <!-- Title -->
            <div class="col-sm-12">
                <div class="title-container">
                    <div class="title-wrap">
                        <h3 class="title">About Universh</h3>
                        <span class="separator line-separator"></span>
                    </div>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam</p>
                </div>
            </div>
            <!-- Title -->
            <div class="col-sm-6 col-md-3">
                <!-- Count Block -->
                <div class="count-block dark bg-verydark">
                    <h5>Department</h5>
                    <h3 data-count="<?php echo $count?>" class="count-number"><span class="counter"><?php echo $count?></span></h3>
                    <i class="uni-fountain-pen"></i>
                </div><!-- Counter Block -->
            </div><!-- Column -->
            <div class="col-sm-6 col-md-3">
                <!-- Count Block -->
                <div class="count-block dark bg-verydark">
                    <h5>Modern lab</h5>
                    <h3 data-count="<?php echo $count?>" class="count-number"><span class="counter"><?php echo $count?></span></h3>
                    <i class="uni-chemical"></i>
                </div><!-- Counter Block -->
            </div><!-- Column -->
            <div class="col-sm-6 col-md-3">
                <!-- Count Block -->
                <div class="count-block dark bg-verydark">
                    <h5>Instructors</h5>
                    <h3 data-count="<?php echo $count?>" class="count-number"><span class="counter"><?php echo $count?></span></h3>
                    <i class="uni-talk-man"></i>
                </div><!-- Counter Block -->
            </div><!-- Column -->
            <div class="col-sm-6 col-md-3">
                <!-- Count Block -->
                <div class="count-block dark bg-verydark">
                    <h5>Students</h5>
                    <h3 data-count="<?php echo $studentcount?>" class="count-number"><span class="counter"><?php echo $count?></span></h3>
                    <i class="uni-brain"></i>
                </div><!-- Counter Block -->
            </div><!-- Column -->
        </div><!-- Row -->
    </div><!-- Container -->
</section><!-- Section -->



<!-- Section -->
<section class="pad-top-none typo-dark">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Title -->
            <div class="col-sm-12">
                <div class="title-container sm">
                    <div class="title-wrap"><br><br>
                        <h3 class="title">Our News</h3>
                        <span class="separator line-separator"></span>
                    </div>
                </div>
            </div>
            <!-- Title -->
            <!-- Column -->
            <?php foreach ($news as  $value) 
            {
                ?>

                <div class="col-sm-4">
                    <!-- Course Wrapper -->
                    <div class="course-wrapper">
                        <!-- Course Banner Image -->
                        <div class="course-banner-wrap">
                            <img width="600" height="220" src="images/<?php echo $value['image'] ?>" class="img-responsive" alt="Course">
                        </div><!-- Course Banner Image -->
                        <!-- Course Detail -->
                        <div class="course-detail-wrap">
                            <!-- Course Content -->
                            <div class="course-content"><br>
                                <h5><?php echo $value['news_heading'] ?></h5>
                                <p><?php echo $value['news_description'] ?><p>
                                    <a class="btn" href="admin/News/show_user_side_single_news/<?php echo $value['id'] ?>">Read More</a>
                                </div><!-- Course Content -->
                            </div><!-- Course Detail -->
                        </div><!-- Course Wrapper -->
                    </div><!-- Column -->
                    <?php 
                }
                ?>
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->
</div><!-- Page Main