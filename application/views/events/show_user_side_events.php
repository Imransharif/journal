<?php 
$date= $events[0]['event_startdate'];
$date_array = explode("-",$date); // split the array
$var_year = $date_array[0]; //day seqment
 $var_month = $date_array[1]; //month segment
 $var_day = $date_array[2]; //year segment 
 ?>
 <!-- Page Main -->
 <div role="main" class="main">
 	<!-- Section -->
 	<section class="full-screen relative typo-light parallax-bg bg-cover" data-background="images/<?php echo $events[0]['event_banner_image']?>"  data-stellar-background-ratio="0.4">
 		<div class="container vmiddle position-none-1024">
 			<div class="row">
 				<div class="col-md-12">
 					<div class="hero hero-scene-event">
 						<h2 class="title"><?php echo $events[0]['banner_heading']?></h2>
 						<h5 class="sub-title"><?php echo $events[0]['banner_paragraph']?>.</h5>
 						<div id="daycounter-hero" class="daycounter clearfix" data-counter="down" data-year="<?php echo $var_year ?>" data-month="<?php echo $var_month ?>" data-date="<?php echo $var_day ?>"></div>

 					</div>
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->

 	<!-- Section -->
 	<section class="typo-dark">
 		<div class="container">
 			<!-- Row -->
 			<div class="row">

 				<!-- Title -->
 				<div class="col-sm-12">
 					<div class="title-container">
 						<div class="title-wrap">
 							<h3 class="title">Upcoming Events</h3>
 							<span class="separator line-separator"></span>
 						</div>
 						<p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam</p>
 					</div>
 				</div>
 				<!-- Title -->

 				<!-- Event Column -->
 				<?php foreach ($events as  $value) 
 				{
 					?>
 					
 					<div class="col-sm-4">
 						<!-- Event Wrapper -->
 						<div class="event-wrap">
 							<div class="event-img-wrap">
 								<img alt="Event" class="img-responsive" src="images/<?php echo $value['event_thumbnail_image']?>" width="600" height="220">
 							</div><!-- Event Image Wrapper -->
 							<!-- Event Detail Wrapper -->
 							<div class="event-details">
 								<h4><a href="event-single-left.html"><?php echo $value['event_name']?></a></h4>
 								<ul class="events-meta">
 									<li><i class="fa fa-calendar-o"></i> <?php echo $value['event_startdate']?></li>
 									<li><i class="fa fa-map-marker"></i> <?php echo $value['event_places']?></li>
 									<a class="btn" href="admin/Events/get_specific_events/<?php echo $value['id'] ?>">Read More</a>
 								</ul>
 								<!-- <a href="event-single-left.html" class="btn">Register Now</a> -->
 							</div><!-- Event Meta -->
 						</div><!-- Event details -->
 					</div><!-- Column -->

 					<?php  
 				} 
 				?>

 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->




 	<!-- Section -->
 	<section class="bg-lgrey typo-dark">
 		<div class="container">

 			<div class="row">
 				<div class="col-sm-6">
 					<!-- Title -->
 					<div class="title-container sm text-left">
 						<div class="title-wrap">
 							<h5 class="title">Why should I go there?</h5>
 							<span class="separator line-separator"></span>
 						</div>
 					</div>
 					<ul class="list-icon">
 						<?php $gothere= $events[0]['gothere'];
 						$gotherearray=explode(',', $gothere);
 						foreach ($gotherearray as  $value) 
 						{
 							?>
 							<li><?php echo $value ?></li>
 							<?php  
 						}

 						?>
 					</ul><!-- List -->
 				</div><!-- Column -->

 				<div class="col-sm-6">
 					<div class="list-img">
 						<img alt="Surity" class="img-responsive img-center" src="assets1/images/default/surity.png" width="441" height="361">
 					</div>
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->

 	<!-- Section -->
 	<section class="typo-dark">
 		<div class="container">
 			<div class="row">
 				<div class="col-sm-6">
 					<div class="list-img">
 						<img alt="Surity" class="img-responsive img-center" src="assets1/images/default/things.png" width="441" height="361">
 					</div>
 				</div><!-- Column -->
 				<div class="col-sm-6">
 					<!-- Title -->
 					<div class="title-container sm text-left">
 						<div class="title-wrap">
 							<h5 class="title">What do i need?</h5>
 							<span class="separator line-separator"></span>
 						</div>
 					</div>
 					<!-- List -->
 					<div class="row">
 						<div class="col-md-6">
 								<ul class="list-icon">
 								<?php $doneed= $events[0]['doneed'];
 								$doneedarray=explode(',', $doneed);
 								foreach ($doneedarray as  $value) 
 								{
 									?>
 									<li><?php echo $value ?></li>
 									<?php  
 								}

 								?>
 							</ul><!-- List -->
 						</div><!-- Column -->
 						<div class="col-md-6">
 							
 						</div><!-- Column -->
 					</div><!-- Row -->	
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->


 </div><!-- Page Main -->

