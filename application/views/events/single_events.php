<?php 
$date= $events[0]['event_startdate'];
$date_array = explode("-",$date); // split the array
$var_year = $date_array[0]; //day seqment
 $var_month = $date_array[1]; //month segment
 $var_day = $date_array[2]; //year segment 
 ?>
 <!-- Page Main -->
 <div role="main" class="main">
 	<!-- Section -->
 	<section class="full-screen relative typo-light parallax-bg bg-cover" data-background="images/<?php echo $events[0]['event_thumbnail_image']?>"  data-stellar-background-ratio="0.4">
 		<div class="container vmiddle position-none-1024">
 			<div class="row">
 				<div class="col-md-12">
 					<div class="hero hero-scene-event">
 						<h2 class="title"><?php echo $events[0]['banner_heading']?></h2>
 						<h5 class="sub-title"><?php echo $events[0]['banner_paragraph']?>.</h5>
 						<div id="daycounter-hero" class="daycounter clearfix" data-counter="down" data-year="<?php echo $var_year ?>" data-month="<?php echo $var_month ?>" data-date="<?php echo $var_day ?>"></div>

 					</div>
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->

 	<!-- Section -->
 	<section class="typo-dark">
 		<div class="container">
 			<!-- Row -->
 			<div class="row">

 				<!-- Title -->
 				<div class="col-sm-12">
 					<div class="title-container">
 						<div class="title-wrap">
 							<h3 class="title"><?php echo $events[0]['event_name']?></h3>
 							<span class="separator line-separator"></span>
 						</div>
 						<p class="description"><?php echo $events[0]['event_desc']?></p>
 					</div>
 				</div>
 				<!-- Title -->



 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->




 	<!-- Section -->
 	<section class="bg-lgrey typo-dark">
 		<div class="container">

 			<div class="row">
 				<div class="col-sm-6">
 					<!-- Title -->
 					<div class="title-container sm text-left">
 						<div class="title-wrap">
 							<h5 class="title">Why should I go there?</h5>
 							<span class="separator line-separator"></span>
 						</div>
 					</div>
 					<!-- List -->
 					<ul class="list-icon">
 						<?php $gothere= $events[0]['gothere'];
 						$gotherearray=explode(',', $gothere);
 						foreach ($gotherearray as  $value) 
 						{
 							?>
 							<li><?php echo $value ?></li>
 							<?php  
 						}

 						?>
 					</ul><!-- List -->
 				</div><!-- Column -->

 				<div class="col-sm-6">
 					<div class="list-img">
 						<img alt="Surity" class="img-responsive img-center" src="assets1/images/default/surity.png" width="441" height="361">
 					</div>
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->

 	<!-- Section -->
 	<section class="typo-dark">
 		<div class="container">
 			<div class="row">
 				<div class="col-sm-6">
 					<div class="list-img">
 						<img alt="Surity" class="img-responsive img-center" src="assets1/images/default/things.png" width="441" height="361">
 					</div>
 				</div><!-- Column -->
 				<div class="col-sm-6">
 					<!-- Title -->
 					<div class="title-container sm text-left">
 						<div class="title-wrap">
 							<h5 class="title">What do i need?</h5>
 							<span class="separator line-separator"></span>
 						</div>
 					</div>
 					<!-- List -->
 					<div class="row">
 						<div class="col-md-6">
 							<ul class="list-icon">
 								<?php $doneed= $events[0]['doneed'];
 								$doneedarray=explode(',', $doneed);
 								foreach ($doneedarray as  $value) 
 								{
 									?>
 									<li><?php echo $value ?></li>
 									<?php  
 								}

 								?>
 							</ul><!-- List -->
 						</div><!-- Column -->
 						<div class="col-md-6">

 						</div><!-- Column -->
 					</div><!-- Row -->	
 				</div><!-- Column -->
 			</div><!-- Row -->
 		</div><!-- Container -->
 	</section><!-- Section -->


 </div><!-- Page Main -->

