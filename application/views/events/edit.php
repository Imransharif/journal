<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Events/update" method="POST" enctype="multipart/form-data">
           <input type="hidden" name="id" value="<?php echo $data[0]['id']?>">
           <input type="hidden" name="banner_image" value="<?php echo $data[0]['event_banner_image']?>">
           <input type="hidden" name="thumbnail_image" value="<?php echo $data[0]['event_thumbnail_image']?>">
           <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Event Name</label>
            <div class="col-sm-10">
              <input type="text" name="event_name" class="form-control form-control-square" id="input-17"  value="<?php echo $data[0]['event_name']?>"  >
            </div>
          </div>
          <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Event Place</label>
            <div class="col-sm-10">
              <input type="text" name="event_places" class="form-control form-control-square" id="input-17" value="<?php echo $data[0]['event_places']?>"  >
            </div>

          </div>
          <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Event Start Date</label>
            <div class="col-sm-10">
              <input type="date" max="0" name="event_startdate" class="form-control form-control-square" id="input-17" value="<?php echo $data[0]['event_startdate']?>" >
            </div>
          </div>
          <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Event End date</label>
            <div class="col-sm-10">
              <input type="date" name="event_enddate" class="form-control form-control-square" id="input-17" value="<?php echo $data[0]['event_enddate']?>"  >
            </div>
          </div>
          <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Banner_heading</label>
            <div class="col-sm-10">
              <input type="text" name="banner_heading" class="form-control form-control-square" id="input-17" value="<?php echo $data[0]['banner_heading']?>"  >
            </div>
          </div>
          <?php $gothere= $data[0]['gothere'];
          $gotherearray=explode(',', $gothere);
          foreach ($gotherearray as  $value) 
          {
            ?>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Why should I go there</label>
              <div class="col-sm-10">
                <input type="text" name="gothere[]" class="form-control form-control-square" id="input-17" value="<?php echo $value?>"  >
              </div>
            </div>
            <?php  
          }

          ?>
          <?php $doneed= $data[0]['doneed'];
                $doneedarray=explode(',', $doneed);
                foreach ($doneedarray as  $value) 
                {
                  ?>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">What do i need?</label>
              <div class="col-sm-10">
                <input type="text" name="doneed[]" class="form-control form-control-square" id="input-17" value="<?php echo $value?>"  >
              </div>
            </div>
            <?php  
          }

          ?>

          <div class="form-group row">
            <label for="placeholder-input" class="col-sm-2 col-form-label">Banner_Paragraph</label>
            <div class="col-sm-10">
              <input type="text" name="banner_paragraph" class="form-control form-control-square" id="input-17" value="<?php echo $data[0]['banner_paragraph']?>"  >
            </div>
          </div>
          <div class="department_desc">
            <div class="form-group row ">
             <label for="input-17" class="col-sm-2 col-form-label ">Description</label>
             <div class="col-sm-10">
              <textarear class="summernote" value="<?php echo $data[0]['event_desc']?>"><?php echo $data[0]['event_desc']?></textarear>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="input-8" class="col-sm-2 col-form-label">Event Banner_image</label>
          <div class="col-sm-10">
            <input type="file" class="form-control" id="input-8" name="banner_image">
            <img src="images/<?php echo $data[0]['event_banner_image']?>" width="150" height="150">
          </div>
        </div>


        <div class="form-group row">
          <label for="input-8" class="col-sm-2 col-form-label">Event thumbnail_image</label>
          <div class="col-sm-10">
            <input type="file" class="form-control" id="input-8" name="thumbnail_image">
            <img src="images/<?php echo $data[0]['event_thumbnail_image']?>" width="150" height="150">
          </div>
        </div>

        

        <div class="form-group">
          <input type="submit" name="submit" class="btn btn-primary">
        </div>

      </form>

    </div>
  </div>
</div>
</div><!--End Row-->
<script type="text/javascript">

</script>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
