<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Events/insertion" method="POST" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Event Name</label>
              <div class="col-sm-10">
                <input type="text" name="event_name" class="form-control form-control-square" id="input-17"  placeholder="Enter Name">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Event Place</label>
              <div class="col-sm-10">
                <input type="text" name="event_places" class="form-control form-control-square" id="input-17"  placeholder="Place">
              </div>

            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Event Start Date</label>
              <div class="col-sm-10">
                <input type="date" min="<?php echo date('Y-m-d')?>" name="event_startdate" class="form-control form-control-square" id="startdate"  placeholder="Password" onchange="ValidateEndDate()" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Event End date</label>
              <div class="col-sm-10">
                <input type="date" min="<?php echo date('Y-m-d')?>" name="event_enddate" class="form-control form-control-square" id="enddate"  placeholder="Phone Number" onchange="ValidateEndDate()" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Banner_heading</label>
              <div class="col-sm-10">
                <input type="text" name="banner_heading" class="form-control form-control-square" id="input-17"  placeholder="Banner_heading">
              </div>
            </div>
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Banner_paragraph</label>
              <div class="col-sm-10">
                <input type="text" name="banner_paragraph" class="form-control form-control-square" id="input-17"  placeholder="Banner_paragraph">
              </div>
            </div>
            <div id="BigButton">
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">Why should I go there?</label>
              <div class="col-sm-9">
                <input type="text" name="gothere[]" class="form-control form-control-square" id="input-17"  placeholder="Why should I go there?">
              </div>
              <div class="col-sm-1">
                <h1 class="btn btn-primary" id="gothereadd" style="padding: 2px 10px; background-color: black; font-size: 20px;">+</h1>
                
              </div>
            </div>
            </div>
            <div id="rightDiv"></div>
            <div id="needadd">
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-2 col-form-label">What do i need?</label>
              <div class="col-sm-9">
                <input type="text" name="doneed[]" class="form-control form-control-square" id="input-17"  placeholder="What do i need?">
              </div>
              <div class="col-sm-1">
                <h1 class="btn btn-primary" id="doineedadd" style="padding: 2px 10px; background-color: black; font-size: 20px;">+</h1>
               
              </div>
            </div>
          </div>
          <div id="afterneedadd"></div>
            <div class="department_desc">
              <div class="form-group row ">
               <label for="input-17" class="col-sm-2 col-form-label ">Enter Events  Description</label>
               <div class="col-sm-10">
                <textarear class="summernote"></textarear>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="input-8" class="col-sm-2 col-form-label">Event_banner_image</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="input-8" name="banner_image" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="input-8" class="col-sm-2 col-form-label">Event thumbnail_image</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="input-8" name="thumbnail_image" required>
            </div>
          </div>



          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary">
          </div>

        </form>

      </div>
    </div>
  </div>
</div><!--End Row-->

<script type="text/javascript">
  function ValidateEndDate() 
  { 
    var startDate = $("#startdate").val();
    var endDate = $("#enddate").val();

    if (startDate != '' && endDate !='') 
    {
      if (Date.parse(startDate) > Date.parse(endDate)) 
      {
       $("#enddate").val('');
       alert("Start date should not be greater than end date");
     }
   }
   return false;
 }
</script>
<script type="text/javascript">
  $(document).on('click','#gothereadd',function()
  {
     $("#BigButton").clone().appendTo("#rightDiv");
  })
</script>
<script type="text/javascript">
  $(document).on('click','#doineedadd',function()
  {
     $("#needadd").clone().appendTo("#afterneedadd");
  })
</script>
</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
