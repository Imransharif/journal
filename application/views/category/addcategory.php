<div class="clearfix"></div>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Form Input</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javaScript:void();">DashRock</a></li>
          <li class="breadcrumb-item"><a href="javaScript:void();">Forms</a></li>
          <li class="breadcrumb-item active" aria-current="page">Form Input</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
         <div class="card-header text-uppercase">Text Input</div>
         <div class="card-body">
          <form action="admin/Category/insertion" method="POST">
            <div class="form-group row">
              <label for="placeholder-input" class="col-sm-3 col-form-label">Main Category</label>
              <div class="col-sm-6">
                <select class="form-group form-control" name="programe_name">
                   <option value="1">Graduate</option>
                   <option value="2">UnderGraduate</option>
                   <option value="3">PostGraduate</option>
               </select>
             </div>
           </div>
           <div class="form-group row">
            <label for="placeholder-input" class="col-sm-3 col-form-label">Sub Category</label>
            <div class="col-sm-6">
             <input type="text" id="placeholder-input" class="form-control" placeholder="Enter Programe Name 1 Address" name="subcategory_name">
           </div>
         </div>
       
       <div class="form-group">
        <input type="submit" name="submit" class="btn btn-primary">
      </div>

    </form>

  </div>
</div>
</div>
</div><!--End Row-->


</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->

