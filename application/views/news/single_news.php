
<!-- Page Header -->
<div class="page-header bg-dark">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- Page Header Wrapper -->
				<div class="page-header-wrapper">
					<!-- Title & Sub Title -->
					<h3 class="title">News</h3>
					<h6 class="sub-title">All you want know</h6>
					<ol class="breadcrumb">
						<li>Home</li>
						<li class="active">News</li>
					</ol><!-- Breadcrumb -->
				</div><!-- Page Header Wrapper -->
			</div><!-- Coloumn -->
		</div><!-- Row -->
	</div><!-- Container -->
</div><!-- Page Header -->

<!-- Page Main -->
<div role="main" class="main">
	<div class="page-default bg-grey typo-dark">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<!-- Blog Column -->
				<div class="col-xs-12 news-single">
					<div class="blog-single-wrap">
						<div class="news-img-wrap">
							<img src="images/<?php echo $single_news[0]['image']?>" class="img-responsive" alt="News" height="600" width="1200">
						</div>
						<!-- Blog Detail Wrapper -->
						<div class="news-single-details">

							<h4 class="title-simple"><?php echo $single_news[0]['news_heading']?></h4>
							<!-- Blog Description -->
							<p><?php echo $single_news[0]['news_description']?></p>

						</div><!-- Blog Detail Wrapper -->
					</div><!-- Blog Wrapper -->
					
					<!-- Blog Share Post -->
				<!-- 	<div class="share">
						<h5>Share Article : </h5>
						<ul class="social-icons color">
							<li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
							<li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
							<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin">Linkedin</a></li>
							<li class="mail"><a href="http://www.gmail.com/" target="_blank" title="mail">mail</a></li>
							<li class="googleplus"><a href="http://www.googleplus.com/" target="_blank" title="googleplus">googleplus</a></li>
							<li class="wordpress"><a href="http://www.wordpress.com/" target="_blank" title="wordpress">wordpress</a></li>
							<li class="instagram"><a href="http://www.instagram.com/" target="_blank" title="instagram">instagram</a></li>
						</ul><!-- Blog Social Share -->
					</div><!-- Blog Share Post --> 
					<?php  
					 if(!empty($remaining_news))
                     {?>
					<!-- Related Post -->
					<h4 class="title-simple">Read More News : </h4>
					<div class="owl-carousel" 
					data-animatein="zoomIn" 
					data-animateout="slideOutDown" 
					data-margin="30" 
					data-stagepadding="" 
					data-loop="true" 
					data-merge="true" 
					data-nav="true"
					data-dots="false" 
					data-items="1"  data-mobile="1" data-tablet="2" data-desktopsmall="2"  data-desktop="3" 
					data-autoplay="true" 
					data-delay="3000" 
					data-navigation="true">
					<?php 
					foreach ($remaining_news as  $value) 
					{
						?>

						<div class="item">
							<!-- Related Wrapper -->
							<div class="related-wrap">
								<!-- Related Image Wrapper -->
								<div class="img-wrap">
						<img width="600" height="220" alt="news" class="img-responsive" src="images/<?php echo $value['image']?>">
								</div>
								<!-- Related Content Wrapper -->
								<div class="related-content">
									<a href="admin/News/show_user_side_single_news/<?php echo $value['id'] ?>" title="Read More">+</a>
									<h5 class="title"><?php echo substr($value['news_heading'],0,20)?></h5>
								</div><!-- Related Content Wrapper -->
							</div><!-- Related Wrapper -->
						</div><!-- Item -->
						<?php  
					} 

                     }
					?>
					
				</div><!-- Related Post -->

			
			</div><!-- Column -->
		</div><!-- Row -->
	</div><!-- Container -->
</div><!-- Page Default -->
</div><!-- Page Main -->

