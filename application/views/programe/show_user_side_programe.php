<!-- Page Main -->
<div role="main" class="main">
<!-- Page Header -->
<div class="page-header bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- Page Header Wrapper -->
                <div class="page-header-wrapper">
                    <!-- Title & Sub Title -->
                    <h3 class="title"><?php 
                            echo $programe;
                            ?></h3>
                    <h6 class="sub-title">All you want know</h6>
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active"><?php 
                            echo $programe;
                            ?></li>
                    </ol><!-- Breadcrumb -->
                </div><!-- Page Header Wrapper -->
            </div><!-- Coloumn -->
        </div><!-- Row -->
    </div><!-- Container -->
</div><!-- Page Header -->
    <!-- Section -->
    <section id="gallery" class="bg-lgrey typo-dark">
        <div class="container">

            <div class="row">
                <!-- Title -->
                <div class="col-sm-12">
                    <div class="title-container text-left">
                        <div class="title-wrap">
                            <h3 class="title"> <?php 
                            echo $programe;
                            ?></h3>
                            <span class="separator line-separator"></span>
                        </div>
                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam</p>
                    </div>
                </div><!-- Title -->
            </div><!-- Row -->
            
            <div class="row">
                <div class="col-md-12">
                    <!-- Filters -->
                    <div class="isotope-filters">
                        <ul class="nav nav-pills">
                            <?php if(!empty($department)) 
                            {
                                ?>
                            <li><a href="#" data-filter=".all" class="filter active">All</a></li>

                            <?php 
                             $category_name='';
                            foreach ($department as  $value) 
                            {
                                
                                if($category_name!=str_replace(' ', '_', $value['category_name']))
                                {
                                ?>

                                <li><a href="#" data-filter=".<?php echo str_replace(' ', '_', $value['category_name']); ?>" class="filter"><?php echo $value['category_name']?></a>
                                </li>

                                <?php
                            }
                            $category_name=str_replace(' ', '_', $value['category_name']);
                            }
                        }
                            ?>
                        </ul>
                    </div><!-- Filters -->
                    <!-- Gallery Block -->
                    <div class="isotope-grid grid-three-column has-gutter-space" data-gutter="20" data-columns="3">
                    <?php
                    foreach ($department as  $value) 
                    {
                        $programe_id=$value['programe_id'];
                        if($programe_id==1)
                        {
                            $programe_name='Graduate';
                        }
                        $id=$value['department_id'];
                        ?>
                        
                            <!-- Portfolio Item -->
                            <div class="item all <?php echo str_replace(' ', '_', $value['category_name']); ?>">
                                <!-- Image Wrapper -->
                                <div class="image-wrapper">
                                    <!-- IMAGE -->
                                    <img src="images/<?php echo $value['image']?>" alt="" height="320px" />
                                    <div aria-multiselectable="true" role="tablist" id="gallery1" class="panel-group accordion gallery-accordion">
                                        <div class="panel panel-default">
                                            <div id="heading1" role="tab" class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a aria-controls="collapse1" aria-expanded="false" href="#collapse1<?php echo $id?>" data-parent="#gallery1" data-toggle="collapse" role="button" class="collapsed">
                                                        <?php echo $value['department_name'];?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div aria-labelledby="heading1" role="tabpanel" class="panel-collapse collapse" id="collapse1<?php echo $id?>" aria-expanded="false">
                                                <div class="panel-body">
                                                    <div class="gallery-detail">
                                                        <p><?php echo substr($value['department_desc'],0,20)?></p>
                                                        <a href="admin/Department/single_department/<?php echo $id?>" class="btn uni-upload-2"></a>
                                                        <a href="images/<?php echo $value['image']?>" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Gallery Accordion -->
                                </div><!-- Image Wrapper -->
                            </div><!-- Portfolio Item -->

                            <!-- Portfolio Item -->
                        

                        <?php                            
                    }
                    ?>
</div><!-- Gallery Block -->
                </div><!-- Column -->
            </div><!-- Row -->
        </div><!-- Container -->
    </section><!-- Section -->

</div><!-- Page Main -->