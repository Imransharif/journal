<!-- Page Header -->
<div class="page-header bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- Page Header Wrapper -->
                <div class="page-header-wrapper">
                    <!-- Title & Sub Title -->
                    <h3 class="title">Alumini</h3>
                    <h6 class="sub-title">All you want know</h6>
                    <ol class="breadcrumb">
                        <li>Home</li>
                        <li class="active">Alumini</li>
                    </ol><!-- Breadcrumb -->
                </div><!-- Page Header Wrapper -->
            </div><!-- Coloumn -->
        </div><!-- Row -->
    </div><!-- Container -->
</div><!-- Page Header -->
 <!-- Section -->
 <br><br><br><br>
 <section class="pad-top-none typo-dark">
    <div class="container">
        <!-- Row -->
        <div class="row">
            <!-- Title -->
            <div class="col-sm-12">
                <div class="title-container sm">
                    <div class="title-wrap">
                        <h3 class="title">Our Alumini Student</h3>
                        <span class="separator line-separator"></span>
                    </div>
                </div>
            </div>
            <!-- Title -->
            <!-- Column -->
            <?php foreach ($alumini as  $value) 
            {
                ?>

                <div class="col-sm-4">
                    <!-- Course Wrapper -->
                    <div class="course-wrapper">
                        <!-- Course Banner Image -->
                        <div class="course-banner-wrap">
                            <img width="600" height="220" src="images/<?php echo $value['image'] ?>" class="img-responsive" alt="Course">
                            <span class="cat bg-yellow"><?php echo $value['programe'] ?></span>
                        </div><!-- Course Banner Image -->
                        <!-- Course Detail -->
                        <div class="course-detail-wrap">
                            <!-- Course Content -->
                            <div class="course-content"><br>
                                <h5><a href="course-single-left.html"><?php echo $value['name'] ?></a></h5>
                                <p><?php echo $value['email'] ?><p>
                                <p><?php echo $value['phone'] ?><p>
                                <p><?php echo $value['address'] ?><p>
                                            <!-- <a class="btn" href="#">Apply Now</a> -->
                                        </div><!-- Course Content -->
                                    </div><!-- Course Detail -->
                                </div><!-- Course Wrapper -->
                            </div><!-- Column -->
                            <?php 
                        }
                        ?>
                    </div><!-- Row -->
                </div><!-- Container -->
    </section><!-- Section -->