<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('topnavbar'))
{
    function Web_owner_info_model()
    {
        
        $CI=& get_instance();    
        $CI->load->model('Web_owner_info_model');
         return  $CI->Web_owner_info_model->get_where('*','',true, 'id DESC', 4,'');
       
    }
}
if ( ! function_exists('related_depertment'))
{
    function related_depertment($id)
    {
        $where = "category_id = '".$id."'";
         $CI=& get_instance();    
        $CI->load->model('Department_model');
         return  $CI->Department_model->get_where('*',$where,true, 'department_id DESC', 3,'');
       
    }
}

if ( ! function_exists('activate_menu'))
{
    function activate_menu($controller) {
        $CI = get_instance();
        $class = $CI->router->fetch_class();
        return ($class == $controller) ? 'active' : '';
    }
}
if ( ! function_exists('activate_sub_menu'))
{

    function activate_sub_menu($controller) {
        $CI = get_instance();
        $class = $CI->router->fetch_class();
        $method = $CI->router->fetch_method();
        $url = "$class/$method";
        return ($url == $controller) ? 'active' : '';
    }
}
