-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2019 at 08:16 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gournal_site`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumini`
--

CREATE TABLE `alumini` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `programe` varchar(255) NOT NULL,
  `cname` varchar(255) NOT NULL,
  `caddress` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumini`
--

INSERT INTO `alumini` (`id`, `name`, `email`, `phone`, `programe`, `cname`, `caddress`, `address`, `image`) VALUES
(5, 'Jeremy Walton', 'mifazamedy@mailinator.net', '+791-82-2530992', 'Impedit ', 'Claudia ', 'Quintessa  DFGH', 'Aut numquam est', '5886628-hotel-images3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `heading`, `description`) VALUES
(8, 'saddfgdfg', '<p>dsfsffdgdfgdg</p>');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `p_id` int(11) NOT NULL,
  `status` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `p_id`, `status`) VALUES
(12, 'Bscs', 2, 0),
(14, 'Css', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `heading`, `description`) VALUES
(8, 'saddfgdfg', '<p>dsfsffdgdfgdg</p>');

-- --------------------------------------------------------

--
-- Table structure for table `coordinator`
--

CREATE TABLE `coordinator` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coordinator`
--

INSERT INTO `coordinator` (`id`, `name`, `email`, `password`, `phone`, `address`, `image`, `status`) VALUES
(3, 'Ivory Whitney', 'qemehidado@mailinator.com', 'rter', '+966-14-7034753', 'Excepturi itaque ', 'bhutan-wallpaper2.jpg', 'Director_Pb');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `department_desc` varchar(1000) NOT NULL,
  `heading` varchar(1000) NOT NULL,
  `paragraph` varchar(1000) NOT NULL,
  `category_id` int(11) NOT NULL,
  `programe_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `dep_status` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `department_desc`, `heading`, `paragraph`, `category_id`, `programe_id`, `image`, `dep_status`) VALUES
(25, 'Hedy Strong', 'ty', 'Price435, Steele1435', '<p>YUYe45</p>,<p>YTU435</p>', 12, 3, 'bhutan-wallpaper27.jpg', 1),
(26, 'Haley Miller', '<p>zxdfds</p>', 'HEADING', '<p>dsfsdf</p>', 12, 3, '40104473-winter-wallpapers26.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `director_pb`
--

CREATE TABLE `director_pb` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `director_pb`
--

INSERT INTO `director_pb` (`id`, `name`, `email`, `password`, `phone`, `address`, `image`, `status`) VALUES
(2, 'Garrison ', 'puqojosu@mailinator.com', 'Aut ', '0303727372', 'Ab iste', '40104473-winter-wallpapers13.jpg', 'Director_Pb');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_places` varchar(255) NOT NULL,
  `event_startdate` date NOT NULL,
  `event_enddate` date NOT NULL,
  `event_banner_image` varchar(255) NOT NULL,
  `banner_heading` varchar(255) NOT NULL,
  `banner_paragraph` varchar(255) NOT NULL,
  `event_desc` varchar(255) NOT NULL,
  `gothere` varchar(255) NOT NULL,
  `doneed` varchar(255) NOT NULL,
  `event_thumbnail_image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_places`, `event_startdate`, `event_enddate`, `event_banner_image`, `banner_heading`, `banner_paragraph`, `event_desc`, `gothere`, `doneed`, `event_thumbnail_image`, `status`) VALUES
(8, 'Eugenia ', 'Dolorem ', '2019-01-01', '2019-01-02', 'bhutan-wallpaper31.jpg', 'Our Latest Event Ready Now!', 'we', '', 'Est in repudiandae,Est in repudiandae ,Est in ', 'dsfdsf', 'b777_200lr_emirates_airlines_412872_aircraft-wallpaper8.jpg', ''),
(9, 'Visit to murree', 'Dolorem ', '2019-01-04', '2019-01-04', 'bhutan-wallpaper32.jpg', 'Our Latest Event Ready Now!', 'Events is helpfull for life', '', 'Est in ,Est in ,Est in', 'do1,do2,do3', '40104473-winter-wallpapers32.jpg', ''),
(10, 'Eugenia Holloway', 'Dolorem exercitationem ', '2019-01-02', '2019-01-09', '40104473-winter-wallpapers33.jpg', 'Our Latest Event Ready Now!', 'Events is helpfull for life', '<p>rwytr</p>', 'Est in ', 'dsfdsf', '42090063_2362402367119741_8233777680269443072_n9.jpg', ''),
(11, 'Eugenia Holloway', 'Dolorem exercitationem ', '2019-01-16', '2019-01-16', '40104473-winter-wallpapers34.jpg', 'Our Latest Event Ready Now!', 'Events is helpfull for life', '<p>dsfs</p>', 'Est in ', 'do1', '42090063_2362402367119741_8233777680269443072_n10.jpg', ''),
(13, 'Eugenia ', 'Dolorem ', '2020-06-23', '2019-02-28', '40104473-winter-wallpapers35.jpg', 'Our Latest Event Ready Now!', 'Events is helpfull for life', '', 'Est in ', 'dsfdsf', '44203326_1250059451811251_1754001932868911104_n.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `code` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `password`, `status`, `code`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', 'admin', 'admin', '', '2018-12-18 05:05:40', '2019-02-22 16:19:16'),
(2, 'user@gmail.com', 'admin', 'user', '', '2018-12-18 05:05:40', '2019-02-22 16:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`id`, `name`, `email`, `password`, `phone`, `address`, `image`, `status`) VALUES
(5, 'Clark Vaughan', 'cofu@mailinator.net', 'Delectus', '0303727372', 'Quia est in laboru', '40104473-winter-wallpapers14.jpg', 'Director_Pb');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `news_heading` varchar(255) NOT NULL,
  `news_description` varchar(255) NOT NULL,
  `news_date` date NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_heading`, `news_description`, `news_date`, `image`) VALUES
(6, 'it is good news for', '<p>i love pakistan fg</p>', '2018-12-13', 'bhutan-wallpaper19.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `course_name`, `file`) VALUES
(2, 'Ryder', 'Php_Developerfor_send1.doc');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `paragraph` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `heading`, `paragraph`, `image`) VALUES
(1, 'Sed ', 'Quod magnam', 'bhutan-wallpaper14.jpg'),
(4, 'England', 'it is good country', 'artsfon_com-848853.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`, `password`, `phone`, `address`, `image`, `status`) VALUES
(2, 'Julie Cook', 'cyxo@mailinator.com', 'Magni', '+392-64-8836245', 'In quo qui minima enim ', '40104473-winter-wallpapers16.jpg', 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriber`
--

INSERT INTO `subscriber` (`id`, `email`) VALUES
(45, 'wed@gmail.com'),
(46, 'wed@gmail.com'),
(47, 'wed@gmail.com'),
(48, 'wed@gmail.com'),
(49, 'wed@gmail.com'),
(50, 'wed@gmail.com'),
(51, 'wed@gmail.com'),
(52, 'wed@gmail.com'),
(53, 'wed@gmail.com'),
(54, ''),
(55, ''),
(56, '');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `name`, `email`, `password`, `phone`, `address`, `image`, `status`) VALUES
(1, 'Walter Allison', 'sunudoxi@mailinator.net', 'tryrty', '+175-42-3337392', 'Veritatis ', 'bhutan-wallpaper8.jpg', 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `vision_mission`
--

CREATE TABLE `vision_mission` (
  `id` int(11) NOT NULL,
  `vision` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vision_mission`
--

INSERT INTO `vision_mission` (`id`, `vision`) VALUES
(3, 'pakistan good');

-- --------------------------------------------------------

--
-- Table structure for table `web_owner_info`
--

CREATE TABLE `web_owner_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `g_plus` varchar(255) NOT NULL,
  `viemo` varchar(255) NOT NULL,
  `linkden` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_owner_info`
--

INSERT INTO `web_owner_info` (`id`, `name`, `logo`, `details`, `phone`, `email`, `password`, `adress`, `facebook`, `instagram`, `twitter`, `g_plus`, `viemo`, `linkden`, `status`) VALUES
(2, 'IMRAN SHARIF', 'IATA.png', 'Officia', '+299-18-7759934', 'admin@gamil.com', 'admin', 'lahore', 'Ab quasi maxime', 'Blanditiis quam ', 'Voluptatem ', 'Sed repellendus', 'Perferendis ', 'Excepteur rerum spit', 'Director_Qec');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumini`
--
ALTER TABLE `alumini`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coordinator`
--
ALTER TABLE `coordinator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `director_pb`
--
ALTER TABLE `director_pb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vision_mission`
--
ALTER TABLE `vision_mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_owner_info`
--
ALTER TABLE `web_owner_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumini`
--
ALTER TABLE `alumini`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `coordinator`
--
ALTER TABLE `coordinator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `director_pb`
--
ALTER TABLE `director_pb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `manager`
--
ALTER TABLE `manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vision_mission`
--
ALTER TABLE `vision_mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `web_owner_info`
--
ALTER TABLE `web_owner_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
